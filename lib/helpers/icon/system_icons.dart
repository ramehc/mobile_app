import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SystemIcon {
  SystemIcon._();

  static IconData back = Platform.isIOS ? SFSymbols.backward : Icons.arrow_back;
  static IconData replay =
      Platform.isIOS ? SFSymbols.arrow_clockwise : Icons.replay;
  static IconData email = Platform.isIOS ? SFSymbols.at : Icons.alternate_email;
  static IconData search = Platform.isIOS ? SFSymbols.search : Icons.search;
  static IconData add = Platform.isIOS ? SFSymbols.plus : Icons.add;
  static IconData check =
      Platform.isIOS ? SFSymbols.checkmark_circle : Icons.check_circle_outline;
  static IconData checkCircle =
      Platform.isIOS ? SFSymbols.checkmark_circle : Icons.check_circle;
  static IconData error =
      Platform.isIOS ? SFSymbols.exclamationmark_circle : Icons.error;
  static IconData help =
      Platform.isIOS ? SFSymbols.question_circle : Icons.help_outline;
  static IconData delete =
      Platform.isIOS ? SFSymbols.trash : Icons.delete_sweep;
  static IconData sort = Icons.sort;
  static IconData close = Platform.isIOS ? SFSymbols.clear : Icons.close;
  static IconData remove = Platform.isIOS ? SFSymbols.minus : Icons.remove;
  static IconData removeCircle =
      Platform.isIOS ? SFSymbols.minus_circle : Icons.remove_circle;
  static IconData bars =
      Platform.isIOS ? SFSymbols.menu : FontAwesomeIcons.bars;
  static IconData bell =
      Platform.isIOS ? SFSymbols.bell : FontAwesomeIcons.bell;
  static IconData info =
      Platform.isIOS ? SFSymbols.info_circle : FontAwesomeIcons.lifeRing;
  static IconData user =
      Platform.isIOS ? SFSymbols.person : FontAwesomeIcons.userCircle;
  static IconData mail =
      Platform.isIOS ? SFSymbols.envelope : Icons.mail_outline;
  static IconData signOut =
      Platform.isIOS ? SFSymbols.arrow_left_square : Icons.exit_to_app;
  static IconData share =
      Platform.isIOS ? SFSymbols.square_arrow_up : Icons.share;
  static IconData tools =
      Platform.isIOS ? SFSymbols.wrench_fill : FontAwesomeIcons.tools;
  static IconData trade =  FontAwesomeIcons.handshake;
  static IconData inbox =
      Platform.isIOS ? SFSymbols.tray_arrow_down : Icons.inbox;
  static IconData more = Platform.isIOS ? SFSymbols.ellipsis : Icons.more_vert;
  static IconData lock = Platform.isIOS ? SFSymbols.lock : Icons.lock;
  static IconData eye = Platform.isIOS ? SFSymbols.eye : Icons.remove_red_eye;
  static IconData setting = Platform.isIOS ? SFSymbols.gear : Icons.settings;
  static IconData portfolio =
      Platform.isIOS ? SFSymbols.chart_pie_fill : FontAwesomeIcons.chartPie;
  static IconData portfolioOutline =
      Platform.isIOS ? SFSymbols.chart_pie : FontAwesomeIcons.chartPie;
  static IconData save =
      Platform.isIOS ? SFSymbols.heart : FontAwesomeIcons.heart;
  static IconData saveFilled =
      Platform.isIOS ? SFSymbols.heart_fill : FontAwesomeIcons.solidHeart;
  static IconData payment =
      Platform.isIOS ? SFSymbols.creditcard : Icons.payment;
  static IconData chartline = Platform.isIOS
      ? SFSymbols.chart_bar_square_fill
      : FontAwesomeIcons.chartLine;
  static IconData chartlineOutline =
      Platform.isIOS ? SFSymbols.chart_bar_square : FontAwesomeIcons.chartLine;
  static IconData document =
      Platform.isIOS ? SFSymbols.doc_text : FontAwesomeIcons.fileAlt;
  static IconData image =
      Platform.isIOS ? SFSymbols.photo_on_rectangle : FontAwesomeIcons.images;
  static IconData feedback =
      Platform.isIOS ? SFSymbols.ellipses_bubble : FontAwesomeIcons.commentDots;
}

