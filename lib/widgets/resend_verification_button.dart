import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/exceptions/error_message_service.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/theme/font_size.dart';

class ResendVerificationLink extends StatefulWidget {
  ResendVerificationLink(
      {Key? key, IAuthenticator? authenticator, ScreenResponseHelper? srh})
      : _authenticator = authenticator ?? GetIt.I<IAuthenticator>(),
        _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
        super(key: key);

  final IAuthenticator _authenticator;
  final ScreenResponseHelper _srh;

  @override
  _ResendVerificationLinkState createState() => _ResendVerificationLinkState();
}

class _ResendVerificationLinkState extends State<ResendVerificationLink> {
  bool isLinkSent = false;
  bool isLoading = false;
  bool get enabled => !isLoading && !isLinkSent;

  IAuthenticator get _authenticator => widget._authenticator;
  ScreenResponseHelper get _srh => widget._srh;
  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return const Padding(
        padding: EdgeInsets.all(8.0),
        child:  CircularProgressIndicator(),
      );
    }

    return ElevatedButton(
      onPressed: enabled
          ? () {
              _authenticator
                  .sendVerificationLink()
                  .then((bool isSent) => setState(() {
                        isLinkSent = isSent;
                        isLoading = false;
                      }))
                  .catchError((e) {
                setState(() {
                  isLoading = false;
                });

                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(Toast.error(ErrorService.getErrorMessage(e)));
              });

              setState(() {
                isLoading = true;
              });
            }
          : null,
      child: SikeText(
        isLinkSent ? 'Email sent' : 'Resend email'
      ),
    );
  }
}
