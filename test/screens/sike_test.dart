// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/splash.dart';
import 'package:sike/screens/login/login.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/sike.dart';
import 'sike_test.mocks.dart';


@GenerateMocks([IAuthenticator, AuthenticationBloc])
void main() {
  late AuthenticationBloc authenticationBloc;
  final ScreenResponseHelper screenResponseHelper = ScreenResponseHelper(411.42857142857144, 797.7142857142857);

  setUp(() {
    authenticationBloc = MockAuthenticationBloc();
    GetIt.I.registerSingleton<ScreenResponseHelper>(screenResponseHelper);
  });

  tearDown((){
    GetIt.I.unregister<ScreenResponseHelper>();
    authenticationBloc.close();
  });

  group('Sike Widget', () {
    testWidgets(
        'Splash screen loads when authentication state is uninitialized',
        (WidgetTester tester) async {
      
      when(authenticationBloc.stream).thenAnswer((_) => Stream.fromIterable([Uninitialized()]));
      when(authenticationBloc.state).thenReturn(Uninitialized());

      await tester.pumpWidget(Sike(
        authenticationBloc: authenticationBloc,
        screenResponseHelper: screenResponseHelper
      ));
      expect(find.byType(SplashScreen), findsOneWidget);
    });

    testWidgets(
        'Login screen loads when authentication state is unauthenticated',
        (WidgetTester tester) async {

      final IAuthenticator authenticator = MockIAuthenticator();
      GetIt.I.registerSingleton<IAuthenticator>(authenticator);

      when(authenticationBloc.stream).thenAnswer((_) => Stream.fromIterable([Unauthenticated()]));
      when(authenticationBloc.state).thenReturn(Unauthenticated());

      await tester.pumpWidget(Sike(
        authenticationBloc: authenticationBloc,
        screenResponseHelper: screenResponseHelper
      ));
      expect(find.byType(LoginScreen), findsOneWidget);

      GetIt.I.unregister<IAuthenticator>();
    });
  });
}
