import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/theme/font_size.dart';
import 'package:sike/theme/fonts.dart';

class SikeText extends StatefulWidget {
  SikeText(this.message, {Key? key, this.maxLines = 1, ScreenResponseHelper? srh,
  this.fontFamily = Fonts.defaultFont,
  this.fontColor = Colors.black
  }) :
  srh = srh ?? GetIt.I<ScreenResponseHelper>(),
   super(key: key);


  final String message;
  final int maxLines;
  final ScreenResponseHelper srh;
  final String fontFamily;
  final Color fontColor;
  @override
  _SikeTextState createState() => _SikeTextState();
}

class _SikeTextState extends State<SikeText> {
  @override
  Widget build(BuildContext context) {
    return Text(
              widget.message,
              maxLines: widget.maxLines,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontFamily: widget.fontFamily,
                fontSize: widget.srh.getRelativeFontSize(FontSize.bodyText),
                color: widget.fontColor
              ));
  }
}