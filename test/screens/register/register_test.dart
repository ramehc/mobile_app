import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/blocs/login/login_bloc.dart';
import 'package:sike/blocs/register/register_bloc.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/constants/authentication_errors.dart';
import 'package:sike/screens/register/register.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/strings.dart';

import 'register_test.mocks.dart';

@GenerateMocks([IAuthenticator, AuthenticationBloc])
void main() {
  LiveTestWidgetsFlutterBinding();
  late RegisterBloc registerBloc;
  late IAuthenticator authenticator;
  late AuthenticationBloc authenticationBloc;
  final ScreenResponseHelper screenResponseHelper =
      ScreenResponseHelper(411.42857142857144, 797.7142857142857);
  late Register register;
  const String email = 'anyvalidemail@gmail.com';
  const String password = 'AnyValidPassword1';
  final Finder registerButton = find.byType(ElevatedButton);

  setUp(() {
    authenticator = MockIAuthenticator();
    authenticationBloc = MockAuthenticationBloc();
    registerBloc = RegisterBloc(authenticator: authenticator);
    GetIt.I.registerSingleton<ScreenResponseHelper>(screenResponseHelper);
    register = Register(
      authenticationBloc: authenticationBloc,
      registerBloc: registerBloc,
      srh: screenResponseHelper,
    );
    when(authenticationBloc.state).thenReturn(Unauthenticated());
    when(authenticationBloc.stream).thenAnswer((_) => Stream.fromIterable([Unauthenticated()]));
  });

  tearDown(() {
    registerBloc.close();
    GetIt.I.unregister<ScreenResponseHelper>();
  });

  Widget makeWidgetTestable(Widget? child) {
    return MaterialApp(
      home: Scaffold(body: child),
    );
  }

  

  Future<void> setEmailField(WidgetTester tester, String email) async{
    final Finder emailField = find.byType(TextFormField).first;

    await tester.tap(emailField);
    await tester.enterText(emailField, email);

    await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));
  }

  Future<void> setPasswordField(WidgetTester tester, String password) async {
    final Finder passwordField = find.byType(TextFormField).last;

    await tester.tap(passwordField);
    await tester.enterText(passwordField, password);

    await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));

  }
  
  Future<void> setEmailAndPasswordField(WidgetTester tester) async {
    await setEmailField(tester, email);
    await setPasswordField(tester, password);
  }

  group('Register Widget', () {
    group(
        'Register button is only enabled when email and password are valid',
        (){

          testWidgets('Register button disabled for invalid email and valid password',  (WidgetTester tester) async{
              await tester.pumpWidget(makeWidgetTestable(register));

              expect(registerButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(registerButton).enabled, isFalse);

              await setEmailField(tester, 'anyinvalidemail');
              await setPasswordField(tester, password);

              await expectLater(tester.widget<ElevatedButton>(registerButton).enabled,
                  isFalse);
          });

           testWidgets('Register button disabled for valid email and invalid password',  (WidgetTester tester) async{
             await tester.pumpWidget(makeWidgetTestable(register));

              expect(registerButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(registerButton).enabled, isFalse);

              await setEmailField(tester, email);
              await setPasswordField(tester, 'anyinvalidpassword');
              
              await expectLater(tester.widget<ElevatedButton>(registerButton).enabled,
                  isFalse);
          });

           testWidgets('Register button enabled for valid email and valid password',  (WidgetTester tester) async{
             await tester.pumpWidget(makeWidgetTestable(register));


              expect(registerButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(registerButton).enabled, isFalse);

              await setEmailAndPasswordField(tester);

              await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));

              await expectLater(tester.widget<ElevatedButton>(registerButton).enabled,
                  isTrue);
          });
    });

    testWidgets('Register with email and password is called when Register button is pressed',  (WidgetTester tester) async{
             await tester.pumpWidget(makeWidgetTestable(register));

              expect(registerButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(registerButton).enabled, isFalse);

              await setEmailAndPasswordField(tester);

              await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));

              await expectLater(tester.widget<ElevatedButton>(registerButton).enabled,
                  isTrue);
              
              when(authenticator.signUp(email, password)).thenAnswer((_) => Future.value(true));

              await tester.tap(registerButton);

              verify(authenticator.signUp(email, password)).called(1);
          });

    group('Correct error message is shown to match register error state', (){

      testWidgets(
        'Invalid email',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(register));
        await setEmailAndPasswordField(tester);
        when(authenticator.signUp(email, password)).thenThrow(PlatformException(code: AuthenticationErrors.invalidEmail));

        await tester.tap(registerButton);
        await tester.pumpAndSettle();

        await expectLater(find.text(Strings.invalidEmail), findsOneWidget);
      });

      testWidgets(
        'Invalid password',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(register));
        await setEmailAndPasswordField(tester);
        when(authenticator.signUp(email, password)).thenThrow(PlatformException(code: AuthenticationErrors.weakPassword));

        await tester.tap(registerButton);
        await tester.pumpAndSettle();

        await expectLater(find.text(Strings.invalidPassword), findsOneWidget);
      });

      testWidgets(
        'Email taken',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(register));
        await setEmailAndPasswordField(tester);
        when(authenticator.signUp(email, password)).thenThrow(PlatformException(code: AuthenticationErrors.emailAlreadyInUse));

        await tester.tap(registerButton);
        await tester.pumpAndSettle();

        await expectLater(find.text(Strings.emailTaken), findsOneWidget);
      });
    });

    group('Sign up successful', (){

      testWidgets(
        'When sign up successful register form is not shown',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(register));
        await setEmailAndPasswordField(tester);
        when(authenticator.signUp(email, password)).thenAnswer((_) => Future.value(true));

        await tester.tap(registerButton);
        await tester.pumpAndSettle();

        await expectLater(find.byType(Form), findsNothing);
      });

      testWidgets( 'When sign up successful success screen is shown', (WidgetTester tester) async{
        await tester.pumpWidget(makeWidgetTestable(register));
        await setEmailAndPasswordField(tester);
        when(authenticator.signUp(email, password)).thenAnswer((_) => Future.value(true));

        await tester.tap(registerButton);
        await tester.pumpAndSettle();

        await expectLater(find.text(Strings.signUpThanks), findsOneWidget);
        expect(find.text('${Strings.verificationSent} $email'), findsOneWidget);
        expect(find.text(Strings.goToSignIn), findsOneWidget);
      });


    });
  });

}
