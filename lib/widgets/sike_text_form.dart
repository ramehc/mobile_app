import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/theme/font_size.dart';

class SikeTextFormField extends StatefulWidget {
  SikeTextFormField({Key? key, ScreenResponseHelper? srh,
    required TextEditingController controller,
    IconData? icon,
    required String labelText, 
    bool obscureText = false,
    AutovalidateMode? autovalidateMode,
    String? Function(String?)? validator,
    bool autoCorrect = true
   }) :
   _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
   _controller = controller,
   _icon = icon,
   _labelText = labelText,
   _obscureText = obscureText,
   _autovalidateMode = autovalidateMode,
   _validator = validator,
   _autoCorrect = autoCorrect,
   super(key: key);

  final TextEditingController _controller;
  final IconData? _icon; 
  final String _labelText;
  final ScreenResponseHelper _srh;
  final bool _obscureText;
  final bool _autoCorrect;
  final AutovalidateMode? _autovalidateMode;
  final String? Function(String?)? _validator;

  @override
  _SikeTextFormFieldState createState() => _SikeTextFormFieldState();
}

class _SikeTextFormFieldState extends State<SikeTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
              controller: widget._controller,
              decoration: InputDecoration(
                icon: widget._icon != null ? Icon(widget._icon) : null,
                labelText: widget._labelText,
                errorStyle: TextStyle(fontSize: widget._srh.getRelativeFontSize(FontSize.bodyText))
              ),
              style: TextStyle(
                fontSize: widget._srh.getRelativeFontSize(FontSize.bodyText)
              ),
              obscureText: widget._obscureText,
              autocorrect: widget._autoCorrect,
              autovalidateMode: widget._autovalidateMode,
              validator: widget._validator,
            );
  }
}

