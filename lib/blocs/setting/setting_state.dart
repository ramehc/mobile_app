part of 'setting_bloc.dart';

abstract class SettingState extends Equatable {
  const SettingState(this.value);
  final String value;
  
  @override
  List<Object> get props => [value];
}

class SettingLoading extends SettingState {
  const SettingLoading() : super('');
}

class SettingLoaded extends SettingState{
  const SettingLoaded(String value): super(value);
}

class SettingError extends SettingState{
  const SettingError(this.errorMessage): assert( errorMessage != ''), super('');
  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}

