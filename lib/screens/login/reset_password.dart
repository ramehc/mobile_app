import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/sike_text_form.dart';
import 'package:sike/helpers/icon/system_icons.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/helpers/validators.dart';
import 'package:sike/constants/authentication_errors.dart';
import 'package:sike/models/route/navigator_pop_result.dart';
import 'package:sike/models/route/pop_result.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/strings.dart';

class ResetPassword extends StatefulWidget {
  ResetPassword(
      {Key? key, IAuthenticator? authenticator, ScreenResponseHelper? srh})
      : _authenticator = authenticator ?? GetIt.I<IAuthenticator>(),
        _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
        super(key: key);

  final IAuthenticator _authenticator;
  final ScreenResponseHelper _srh;

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final TextEditingController _emailController = TextEditingController();
  IAuthenticator get _authenticator => widget._authenticator;
  ScreenResponseHelper get _srh => widget._srh;

  bool get isPopulated => _emailController.text.isNotEmpty;
  bool isSubmitting = false;
  String email = '';

  @override
  void initState() {
    super.initState();
    _emailController.addListener(_onEmailChanged);
  }

  void _onEmailChanged() {
    setState(() {
      email = _emailController.text;
    });
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: AppBar(
          title: const Text(Strings.resetPassword),
        ), //TODO Font size
        body: Builder(
          builder: (BuildContext context) {
            return Padding(
              padding: EdgeInsets.all(_srh.getRelativeWidth(20)),
              child: Form(
                child: ListView(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.all(_srh.getRelativeHeight(12)),
                        child: SikeText(
                          Strings.resetPasswordMessage,
                        )),
                    SikeTextFormField(
                      controller: _emailController,
                      labelText: Strings.email,
                      icon: SystemIcon.email,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autoCorrect: false,
                      validator: (_) {
                        return !_isEmailValid() ? Strings.invalidEmail : null;
                      },
                    ),
                    Container(
                        margin:
                            EdgeInsets.only(top: _srh.getRelativeHeight(30)),
                        child: ElevatedButton(
                            onPressed: _isEmailValid()
                                ? () => _onFormSubmitted(context)
                                : null,
                            child: SikeText(
                              Strings.submit,
                            ))),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> _onFormSubmitted(BuildContext context) async {
    try {
      await _authenticator.sendRestPasswordEmail(email: _emailController.text);
      Navigator.of(context).pop(NavigatorPopResult(
          displayMessage: Strings.resetPasswordLinkSent,
          result: PopResult.success));
    } catch (e) {
      if (e is PlatformException) {
        String? message = e.message;
        switch (e.code) {
          case AuthenticationErrors.userNotFound:
            message = Strings.userNotFound;
            break;
          case AuthenticationErrors.invalidEmail:
            message = Strings.invalidEmail;
        }
        ScaffoldMessenger.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(Toast.error(message ?? Strings.errorSendingRestLink));
        return;
      }

      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(Toast.error(Strings.errorSendingRestLink));
    }
  }

  bool _isEmailValid() {
    return !isSubmitting &&
        isPopulated &&
        email.isNotEmpty &&
        Validators.isValidEmail(email);
  }
}
