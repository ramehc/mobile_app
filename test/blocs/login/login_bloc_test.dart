import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/login/login_bloc.dart';
import 'package:sike/services/authentication/authenticator.dart';

import 'login_bloc_test.mocks.dart';

@GenerateMocks([IAuthenticator])
void main() {

  late LoginBloc loginBloc;
  late IAuthenticator authenticator;
  const String email = 'any';
  const String password = 'any';

  setUp((){
    authenticator = MockIAuthenticator();
    loginBloc = LoginBloc(authenticator: authenticator);
  });

  tearDown((){
    loginBloc.close();
  });

  group('Login Bloc', (){
    test('Initial state for login bloc is empty', (){
      expect(loginBloc.state, LoginState.empty());
    });

    group('loginWithGoogle()', (){

      blocTest('exception thown yields login failure',
      build: () {
        when(authenticator.signInWithGoogle()).thenAnswer((_) => Future.error(Exception()));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(LoginWithGoogle()),
      expect: () => [LoginState.loading(), isA<LoginState>().having((state) => state.isFailure, 'isFailure', isTrue) ]
      );

      blocTest('sign in fails yields login loading, then login failure',
      build: () {
        when(authenticator.signInWithGoogle()).thenAnswer((_) => Future.value(false));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(LoginWithGoogle()),
      expect: () => [LoginState.loading(), isA<LoginState>().having((state) => state.isFailure, 'isFailure', isTrue)]
      );

      blocTest('sign in successful yields login loading, then login success',
      build: () {
        when(authenticator.signInWithGoogle()).thenAnswer((_) => Future.value(true));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(LoginWithGoogle()),
          expect: () => [LoginState.loading(), LoginState.success()]);

    });

    group('loginWithFacebook', () {

      blocTest('exception thown yields login failure', 
      build: (){
        when(authenticator.signInWithFacebook()).thenAnswer((_) => Future.error(Error()));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(LoginWithFacebook()),
      expect: () => [LoginState.loading(), isA<LoginState>().having((state) => state.isFailure, 'isFailure', isTrue)]
      );

      blocTest('sign fails yields login loading, then login failure', 
      build: (){
        when(authenticator.signInWithFacebook()).thenAnswer((_) => Future.value(false));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(LoginWithFacebook()),
      expect: () => [LoginState.loading(), isA<LoginState>().having((state) => state.isFailure, 'isFailure', isTrue)]
      );

      blocTest('sign sucessful yields login loading, then login success', 
      build: (){
        when(authenticator.signInWithFacebook()).thenAnswer((_) => Future.value(true));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(LoginWithFacebook()),
      expect: () => [LoginState.loading(), LoginState.success()]
      );


    });

    group('loginWithCredentials', () {

      //TODO do the other specific exceptions

      blocTest('exception thown yields login failure', 
      build: (){
        when(authenticator.signInWithCredentials(email, password)).thenAnswer((_) => Future.error(Error()));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(const LoginWithCredentials(email: email, password: password)),
      expect: () => [LoginState.loading(), isA<LoginState>().having((state) => state.isFailure, 'isFailure', isTrue)]
      );

      blocTest('sign fails yields login loading, then login failure', 
      build: (){
        when(authenticator.signInWithCredentials(email, password)).thenAnswer((_) => Future.value(false));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(const LoginWithCredentials(email: email, password: password)),
      expect: () => [LoginState.loading(), isA<LoginState>().having((state) => state.isFailure, 'isFailure', isTrue)]
      );

      blocTest('sign sucessful yields login loading, then login success', 
      build: (){
        when(authenticator.signInWithCredentials(email, password)).thenAnswer((_) => Future.value(true));
        return loginBloc;
      },
      act: (dynamic bloc) => bloc.add(const LoginWithCredentials(email: email, password: password)),
      expect: () => [LoginState.loading(), LoginState.success()]
      );

      //TODO test password changed
      //test('')

      //TODO test email changed


    });

  });
  
}