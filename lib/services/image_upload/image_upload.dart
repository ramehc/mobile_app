import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sike/blocs/setting/concrete/profile_picture_bloc.dart';
import 'package:sike/blocs/setting/concrete/sike_picture_bloc.dart';
import 'package:sike/blocs/setting/setting_bloc.dart';
import 'package:sike/helpers/icon/flat_icons.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/services/file_upload/file_upload.dart';
import 'package:sike/services/file_upload/file_upload_event.dart';
import 'package:sike/services/file_upload/firebase_file_upload.dart';
import 'package:sike/services/user_settings/settings_service.dart';
import 'package:sike/strings.dart';
import 'package:sike/theme/font_size.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/toast.dart';

enum ImageType{
  profile,
  sike
}

class ImageUpload {

  ///Takes a setting bloc of type ProfilePictureBloc or SikePictureBloc
  ImageUpload(this._context, this._imageType, this._settingBloc, {UserSettingsService? settingsService, ScreenResponseHelper? srh}):
   _settingsService = settingsService ?? GetIt.I<UserSettingsService>(),
   _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
   assert(_settingBloc.runtimeType == ProfilePictureBloc || _settingBloc.runtimeType == SikePictureBloc);

  final BuildContext _context;
  final ImageType _imageType;
  final SettingBloc _settingBloc;
  final UserSettingsService _settingsService;
  final ImagePicker _imagePicker = ImagePicker();
  final ScreenResponseHelper _srh;


  Future<void> pickImage() async {
    File? response = await imagePicker();

    final bool androidDataLost = Platform.isAndroid && response == null;

    if(androidDataLost){
        final LostData lostData = await _imagePicker.getLostData();
        if(lostData.isEmpty || lostData.exception != null){
          return;
        }

        if(lostData.type != RetrieveType.image){
          return;
        }

        if(lostData.file == null){
          return;
        }

        response = File(lostData.file!.path);
    }
    
    if(response == null){
      return;
    }

    //TODO check crop display on iOS
    final File? croppedFile = await ImageCropper.cropImage(
      sourcePath: response.path,
      cropStyle: ImageType.profile == _imageType ? CropStyle.circle : CropStyle.rectangle,
      aspectRatio:  _imageType == ImageType.profile ? const CropAspectRatio(ratioX: 1, ratioY: 1) : const CropAspectRatio(ratioX: 4, ratioY: 5),
      androidUiSettings:  const AndroidUiSettings(
        initAspectRatio:  CropAspectRatioPreset.square,
        toolbarTitle: Strings.cropImage,
        showCropGrid: true,
        lockAspectRatio: true
      ),
      iosUiSettings: const IOSUiSettings(
        aspectRatioPickerButtonHidden: true,
        aspectRatioLockEnabled: true,
        resetAspectRatioEnabled: false,
      )
    );

    if(croppedFile == null){
      return;
    }

    final String userId = _settingsService.getUserId();
    final FileUpload fileUpload = FirebaseFileUpload(croppedFile, _imageType == ImageType.profile ? '$userId/profilePicture' : '$userId/sikePicture');
    final Stream<FileUploadEvent> fileUploadStream = fileUpload.upload();

    fileUploadStream.listen((FileUploadEvent event){
      switch (event.type) {
        case FileUploadEventType.progress:
          _settingBloc.add(SettingUpdating());
          break;
        case FileUploadEventType.success:
          _settingBloc.add(SettingChanged(event.snapShot.url!));
          break;
        case FileUploadEventType.failure:
          _settingBloc.add(SettingUpdateError(event.snapShot.errorMessage!));
          break;
      }

    });

  }

  Future<File?> imagePicker() {
    return showModalBottomSheet<File?>(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(25), topLeft: Radius.circular(25))),
      context: _context,
      builder: (BuildContext context) {
        return Wrap(
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text(_imageType == ImageType.profile ? Strings.profilePhoto: Strings.sikePhoto, style: TextStyle(fontWeight: FontWeight.bold, fontSize: _srh.getRelativeFontSize(FontSize.itemTitle)),),
              )
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _optionButton(FlatIcons.picture_2, Strings.gallery, () => _selectImage(ImageSource.gallery)),
                _optionButton(FlatIcons.photoCamera_1, Strings.camera, () => _selectImage(ImageSource.camera)),
                _optionButton(FlatIcons.trash_1, Strings.remove, (){})
              ],
            ),
            Center(
              child: MaterialButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(Strings.cancel, style: TextStyle(fontSize: _srh.getRelativeFontSize(FontSize.itemTitle)),),
              ),
            )
          ],
        );
      });
  }

  Future<void> _selectImage(ImageSource source) async{
    try {
      final PickedFile? pickedFile = await _imagePicker.getImage(
        source: source
      );

      if(pickedFile != null){
        Navigator.of(_context).pop(File(pickedFile.path));
      }

      Navigator.of(_context).pop();
    } catch (e) {
      ScaffoldMessenger.of(_context)..hideCurrentSnackBar()..showSnackBar(Toast.error(Strings.errorSelectingImage));
      Navigator.of(_context).pop();
    }
   
  }

  Widget _optionButton(IconData iconData, String label, Function onpress){
    return GestureDetector(
      onTap:()=> onpress(),
      child: Padding(
        padding: const EdgeInsets.only(top: 15, bottom: 12),
        child: Column(
          children: <Widget>[
            Icon(iconData, size: 32,),
            SikeText(label)
          ],
        ),
      ),
    );
  }
}