import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/setting/concrete/display_name_bloc.dart';
import 'package:sike/blocs/setting/concrete/profile_picture_bloc.dart';
import 'package:sike/blocs/setting/concrete/sike_picture_bloc.dart';
import 'package:sike/blocs/setting/setting_bloc.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/constants/image_assets.dart';
import 'package:sike/constants/routes.dart';
import 'package:sike/screens/home/cards.dart';
import 'package:sike/helpers/icon/system_icons.dart';

class Home extends StatefulWidget {
  Home(
      {Key? key,
      ScreenResponseHelper? srh,
      ProfilePictureBloc? profilePictureBloc,
      DisplayNameBloc? displayNameBloc,
      SikePictureBloc? sikePictureBloc
      }):
        _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
        _profilePictureBloc = profilePictureBloc,
        _displayNameBloc = displayNameBloc,
        _sikePictureBloc = sikePictureBloc,
        super(key: key);

  final ScreenResponseHelper _srh;
  final ProfilePictureBloc? _profilePictureBloc;
  final DisplayNameBloc? _displayNameBloc;
  final SikePictureBloc? _sikePictureBloc;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ScreenResponseHelper get _srh => widget._srh;
  late ProfilePictureBloc _profilePictureBloc;
  late DisplayNameBloc _displayNameBloc;
  late SikePictureBloc _sikePictureBloc;

  @override
  void initState() {
    super.initState();
    _profilePictureBloc = widget._profilePictureBloc ?? ProfilePictureBloc();
    _profilePictureBloc.add(SettingInitialized());

    _displayNameBloc = widget._displayNameBloc ?? DisplayNameBloc();
    _displayNameBloc.add(SettingInitialized());

    _sikePictureBloc = widget._sikePictureBloc ?? SikePictureBloc();
    _sikePictureBloc.add(SettingInitialized());

    GetIt.instance.registerSingleton<ProfilePictureBloc>(_profilePictureBloc);
    GetIt.instance.registerSingleton<SikePictureBloc>(_sikePictureBloc);
    GetIt.instance.registerSingleton<DisplayNameBloc>(_displayNameBloc);
  }

  @override
  void dispose() {
    GetIt.instance.unregister<ProfilePictureBloc>(
        disposingFunction: (ProfilePictureBloc bloc) => bloc.close());
    GetIt.instance.unregister<SikePictureBloc>(
        disposingFunction: (SikePictureBloc bloc) => bloc.close());
    GetIt.instance.unregister<DisplayNameBloc>(
        disposingFunction: (DisplayNameBloc bloc) => bloc.close());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: GestureDetector(
            onTap: () => Navigator.of(context)
                .pushNamed(Routes.profile),
            child: BlocBuilder<ProfilePictureBloc, SettingState>(
                bloc: _profilePictureBloc,
                builder: (BuildContext context, SettingState state) {
                  return _profilePicture(state);
                }),
          ),
        ),
        body: Cards());
  }

  Widget _profilePicture(SettingState state) {
    if (state.value.isEmpty) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
            decoration: const BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    fit: BoxFit.contain, image: AssetImage(ImageAssets.defaultUser))),
            ),
      );
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: CachedNetworkImage(
        imageUrl: state.value,
        imageBuilder: (BuildContext context, ImageProvider imageProvider) =>
            Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.fill,
            ),
          ),
        ),
        placeholder: (BuildContext context, String url) =>
            const CircularProgressIndicator(),
        errorWidget: (BuildContext context, String url, dynamic error) =>
            Icon(SystemIcon.error),
      ),
    );
  }
}
