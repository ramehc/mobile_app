import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/services/user_settings/settings_service.dart';

part 'setting_event.dart';
part 'setting_state.dart';

abstract class SettingBloc extends Bloc<SettingEvent, SettingState> {
  SettingBloc(UserSettingsService? userSettings) : userSettings = userSettings ?? GetIt.I<UserSettingsService>(),  super(const SettingLoading());

  @protected
  final UserSettingsService userSettings;

  @override
  Stream<SettingState> mapEventToState(
    SettingEvent event,
  ) async* {
    switch (event.runtimeType) {
      case SettingChanged:
        yield* settingChanged(event as SettingChanged);
        break;
      case SettingUpdateError:
        yield* _updateError(event as SettingUpdateError);
        break;
      case SettingChanging:
        yield* _settingChanging();
        break;
      case SettingInitialized:
        yield* getSetting();
        break;
      case SettingUpdating:
        yield const SettingLoading();
        break;
    }
  }

  Stream<SettingState> _settingChanging() async*{
    yield const SettingLoading();
  }

  Stream<SettingState> settingChanged(SettingChanged event);

  Stream<SettingState> getSetting();

  Stream<SettingState> _updateError(SettingUpdateError event) async*{
    yield SettingError(event.errorMessage);
  }
}
