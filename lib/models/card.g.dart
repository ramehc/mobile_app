// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SikeCard _$SikeCardFromJson(Map<String, dynamic> json) {
  return SikeCard(
    id: json['id'] as String,
    title: json['title'] as String,
    description: json['description'] as String,
    imageUrl: json['imageUrl'] as String,
    instructions: json['instructions'] as String,
    maxResponseCharacters: json['maxResponseCharacters'] as int,
    assetURI: json['assetURI'] as String?,
  );
}

Map<String, dynamic> _$SikeCardToJson(SikeCard instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'imageUrl': instance.imageUrl,
      'assetURI': instance.assetURI,
      'maxResponseCharacters': instance.maxResponseCharacters,
      'instructions': instance.instructions,
    };
