// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SikeUser _$SikeUserFromJson(Map<String, dynamic> json) {
  return SikeUser(
    email: json['email'] as String,
    id: json['id'] as String,
    displayName: json['displayName'] as String?,
    profilePictureUrl: json['profilePictureUrl'] as String?,
    sikePictureUrl: json['sikePictureUrl'] as String?,
  );
}

Map<String, dynamic> _$SikeUserToJson(SikeUser instance) => <String, dynamic>{
      'email': instance.email,
      'id': instance.id,
      'displayName': instance.displayName,
      'profilePictureUrl': instance.profilePictureUrl,
      'sikePictureUrl': instance.sikePictureUrl,
    };
