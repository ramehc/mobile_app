import 'package:sike/models/user.dart';

abstract class IAuthenticator {
  Future<bool> signInWithGoogle();
  Future<bool> signInWithFacebook();
  Future<bool> signInWithCredentials(String email, String password);
  Future<bool> signUp(String email, String password);
  Future<bool> signOut();
  ///Returns true if a user is signed in and false if not
  bool isSignedIn();
  ///Returns current user or null if no user is currently signed in
  SikeUser? getUser();
  Future<bool> sendVerificationLink();
  Future<void> sendRestPasswordEmail({required String email});
}
