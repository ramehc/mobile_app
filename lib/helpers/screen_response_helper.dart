import 'package:flutter/material.dart';

class ScreenResponseHelper{ 
  ScreenResponseHelper(double width, double height, [BuildContext? context]){
    _prototypeWidth = width;
    _prototypeHeight = height;

    if(context != null){
      setContext(context);
    }else{
      _height = height;
      _width = width;
      _scaleFactor = 1;
      _scaledHeight = 1;
      _scaledWidth = 1;
    }
    
  }


  
  late double _width;
  late double _height;
  late double _scaleFactor;
  late double _scaledHeight;
  late double _scaledWidth;
  late double _prototypeHeight;
  late double _prototypeWidth;

   void setContext(BuildContext context){
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _scaleFactor = MediaQuery.textScaleFactorOf(context);
    _scaledHeight = _height/ _prototypeHeight;
    _scaledWidth = _width / _prototypeWidth;
  }

  double getScreenWidth(){
    return _width;
  }

  double getPercentageWidth(double percentage){
    return _width * percentage;
  }

  double getScreenHeight(){
    return _height;
  }

  double getPercentageHeight(double percentage){
    return _height * percentage;
  }

  ///Gives width relative to screen size for responsiveness
  double getRelativeWidth(double width){
    return width * _scaledWidth;
  }
  double getRelativeHeight(double height){
    return height * _scaledHeight;
  }
  double getRelativeFontSize(double fontSize){
    if(_scaleFactor < 1){
      return _scaleFactor >= 0.75 ? getRelativeHeight(fontSize) : getRelativeHeight(fontSize) * (1/_scaleFactor) * 0.75;
    }

    return _scaleFactor <= 1.3 ? getRelativeHeight(fontSize) : getRelativeHeight(fontSize) * (1/_scaleFactor) * 1.3;
  }

  bool istextScaled(){
    return _scaleFactor != 1;
  }

  double getScaleHeight(double height){
    return _scaleFactor > 1.3 ? 1.3 * height: height * _scaleFactor;
  }

  double? getScaleFactor(){
    return _scaleFactor;
  }

}