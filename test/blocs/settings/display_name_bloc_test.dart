import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/setting/concrete/display_name_bloc.dart';
import 'package:sike/blocs/setting/setting_bloc.dart';
import 'package:sike/services/user_settings/settings_service.dart';

import 'display_name_bloc_test.mocks.dart';

@GenerateMocks([UserSettingsService])
void main() {
  group('Setting Bloc', () {
    late UserSettingsService userSettingsService;
    late SettingBloc displayNameBloc;

    setUp(() {
      userSettingsService = MockUserSettingsService();
      displayNameBloc = DisplayNameBloc(userSettings: userSettingsService);
    });

    tearDown(() {
      Future.sync(() => displayNameBloc.close());
    });

    test('Bloc intializes with state empty', () {
      expect(displayNameBloc.state, const SettingLoading());
    });

    group('Setting Updating Event', (){
      blocTest('Setting updating event yields settings loading state',
       build: () => displayNameBloc,
       act: (dynamic bloc) => bloc.add(SettingUpdating()),
       expect: () => [const SettingLoading()]
      );
    });

    group('Setting Changed Event', (){
      blocTest('Setting changed event yields settings loading state',
       build: () => displayNameBloc,
       act: (dynamic bloc) => bloc.add(SettingUpdating()),
       expect: () => [const SettingLoading()]
      );
    });

  });
}
