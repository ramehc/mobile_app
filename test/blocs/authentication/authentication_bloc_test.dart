import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/models/user.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/services/local_store/storage_service.dart';

import 'authentication_bloc_test.mocks.dart';


class MockLocalStorageService extends Mock implements ILocalStorageService {}

@GenerateMocks([IAuthenticator])
void main() {
  group('Authentication Bloc', () {
    late IAuthenticator authenticator;
    ILocalStorageService localStorageService;
    late AuthenticationBloc authenticationBloc;
    final SikeUser user = SikeUser(email: 'test', id: 'testId');
    const String errorMessage = 'anything';

    setUp(() {
      authenticator = MockIAuthenticator();
      localStorageService = MockLocalStorageService();
      authenticationBloc = AuthenticationBloc(
          authenticator: authenticator,
          localStorageService: localStorageService);
    });

    tearDown(() {
      Future.sync(() => authenticationBloc.close());
    });

    test('Bloc intializes with state Uninitialized', () {
      expect(authenticationBloc.state, Uninitialized());
    });

    group('AppStarted Event', () {
      blocTest('When no user is signed in bloc emits Unauthenticated',
          build: () {
            when(authenticator.getUser()).thenReturn(null);
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(AppStarted()),
          expect: () => [Unauthenticated()] );

      blocTest(
          'When user is signed in bloc emits Authenticated with current user',
          build: () {
            when(authenticator.getUser()).thenReturn(user);
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(AppStarted()),
          expect: () => [Authenticated(user)]);

      blocTest('When an error is thrown bloc emits AuthenticationError',
          build: () {
            when(authenticator.getUser()).thenThrow(Exception);
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(AppStarted()),
          expect: () => [isA<AuthenticationError>()]);
    });

    group('LoggedIn Event', () {

      blocTest('When an error is thrown bloc emits AuthenticationError',
          build: () {
            when(authenticator.getUser()).thenThrow(Exception);
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(LoggedIn()),
          expect: () => [isA<AuthenticationError>()]);
      blocTest(
          'Bloc emits Unauthenticated and AuthenticationError when user is empty',
          build: () {
            when(authenticator.getUser()).thenReturn(null);
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(LoggedIn()),
          expect: () => [Unauthenticated(), isA<AuthenticationError>()]);

      
      blocTest(
          'Bloc emits Authenticated with current user when user is signed in',
          build: () {
            when(authenticator.getUser()).thenReturn(user);
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(LoggedIn()),
          expect: () => [Authenticated(user)]);
      
    });

    group('LoggedOut Event', () {

      blocTest('When an error is thrown bloc emits AuthenticationError',
          build: () {
            when(authenticator.getUser()).thenThrow(Exception);
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(LoggedOut()),
          expect: () => [isA<AuthenticationError>()]);
      blocTest(
          'Bloc emits AuthenticationError if sign out fails',
          build: () {
            when(authenticator.signOut()).thenAnswer((_) async => Future.value(false));
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(LoggedOut()),
          expect: () => [isA<AuthenticationError>()]);

      blocTest(
          'Bloc emits Unauthenticated if sign out is successful',
          build: () {
            when(authenticator.signOut()).thenAnswer((_) => Future.value(true));
            return authenticationBloc;
          },
          act: (dynamic bloc) => bloc.add(LoggedOut()),
          expect: () => [Unauthenticated()]);
      
    });

     group('LoginError Event', () {

      blocTest('Bloc emits AuthenticationError with error message',
          build: () => authenticationBloc,
          act: (dynamic bloc) => bloc.add(LoginError(errorMessage: errorMessage)),
          expect: () => [AuthenticationError(errorMessage: errorMessage)]);
      
    });
  });
}
