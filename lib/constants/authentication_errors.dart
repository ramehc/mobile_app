abstract class AuthenticationErrors {
  static const String wrongPassword = 'ERROR_WRONG_PASSWORD';
  static const String userDisabled = 'ERROR_USER_DISABLED';
  static const String invalidEmail = 'ERROR_INVALID_EMAIL';
  static const String userNotFound = 'ERROR_USER_NOT_FOUND';
  static const String tooManyAttempts = 'ERROR_TOO_MANY_REQUESTS';
  static const String emailAlreadyInUse = 'ERROR_EMAIL_ALREADY_IN_USE';
  static const String operationNotAllowed = 'ERROR_OPERATION_NOT_ALLOWED';
  static const String weakPassword = 'ERROR_WEAK_PASSWORD';
}