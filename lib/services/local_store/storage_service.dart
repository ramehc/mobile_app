import 'package:sike/services/local_store/preference.dart';

abstract class ILocalStorageService {
  dynamic getFromLocalStorage(Preference preference);
  void saveToLocalStorage(Preference preference, dynamic value);
  Future<bool> removeFromLocalStorage(Preference key);
  bool valueExist(Preference key);
  Future<bool> clear();
}
