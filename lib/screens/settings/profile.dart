import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/constants/routes.dart';
import 'package:sike/helpers/icon/flat_icons.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/screens/settings/display_name.dart';
import 'package:sike/screens/settings/picture.dart';
import 'package:sike/services/image_upload/image_upload.dart';
import 'package:sike/strings.dart';
import 'package:sike/widgets/sike_text.dart';


class Profile extends StatelessWidget {
  Profile({Key? key, ScreenResponseHelper? srh, AuthenticationBloc? authenticationBloc}):
  srh = srh ?? GetIt.I<ScreenResponseHelper>(),
  authenticationBloc = authenticationBloc ?? GetIt.I<AuthenticationBloc>(),
    super(key: key);
  final ScreenResponseHelper srh;
  final AuthenticationBloc authenticationBloc;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
          appBar: AppBar(
            elevation: 0,
          ),
          backgroundColor: Colors.white,
          body: Column(
            children: <Widget>[
              Expanded(
                child: _body(),
              ),
              Container(
                child: _bottomSheet(context),
              )
            ],
          )),
    );
  }

  Widget _body() {
    return ListView(
          children: <Widget>[ Column(
        children: <Widget>[
          const UserImage(imageType: ImageType.profile,),
          SizedBox(height: srh.getPercentageHeight(0.02)),
          DisplayName(),
          SizedBox(height: srh.getPercentageHeight(0.02)),
          const UserImage(imageType: ImageType.sike,)
        ],
      )]
    );
  }

  Material _bottomSheet(BuildContext context) {
    return Material(
      color: Colors.white,
      elevation: 15,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          TextButton.icon(
            icon: const Icon(
              FlatIcons.settings_3,
              color: Colors.black
            ),
            label: SikeText(Strings.settings),
            onPressed: () => Navigator.of(context).pushNamed(Routes.settings),
          ),
          Container(
            height: srh.getPercentageHeight(0.0304),
            color: Colors.grey,
            width: 1,
          ),
          TextButton.icon(
            icon: const Icon(FlatIcons.logout_2, color: Colors.black),
            label: SikeText(Strings.signOut),
            onPressed: () {
              authenticationBloc.add(LoggedOut());
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    );
  }
}

//TODO Add authentication bloc listener to listen for errors in logging out
