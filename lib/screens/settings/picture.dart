import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/setting/concrete/profile_picture_bloc.dart';
import 'package:sike/blocs/setting/concrete/sike_picture_bloc.dart';
import 'package:sike/blocs/setting/setting_bloc.dart';
import 'package:sike/services/image_upload/image_upload.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/widgets/user_image_display.dart';

class UserImage extends StatefulWidget {
  const UserImage({Key? key, required this.imageType}) : super(key: key);
  final ImageType imageType;

  @override
  _UserImageState createState() => _UserImageState();
}

class _UserImageState extends State<UserImage> {
  late SettingBloc _pictureBloc;

  @override
  void initState() {
    super.initState();
    _pictureBloc = widget.imageType == ImageType.profile
        ? GetIt.instance<ProfilePictureBloc>()
        : GetIt.instance<SikePictureBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () async {
          await ImageUpload(context, widget.imageType, _pictureBloc)
              .pickImage();
        },
        child: BlocListener<SettingBloc, SettingState>(
          bloc: _pictureBloc,
          listener: (BuildContext context, SettingState state) {
            if (state is SettingError) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(Toast.error(state.errorMessage));
            }
          },
          child: BlocBuilder<SettingBloc, SettingState>(
            bloc: _pictureBloc,
            builder: (BuildContext context, SettingState state) {
              return UserImageDisplay(widget.imageType, imageUrl: state.value,);
            },
          ),
        ));
  }
}
