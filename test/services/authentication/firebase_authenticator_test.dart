import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/exceptions/exceptions.dart';
import 'package:sike/models/user.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/services/authentication/firebase_authenticator.dart';
import 'package:sike/strings.dart';

import 'firebase_authenticator_test.mocks.dart';

@GenerateMocks([AccessToken, FacebookAuthenticationProvider, GoogleAuthenticationProvider, GoogleSignInAuthentication, GoogleSignInAccount, OAuthCredential,
UserCredential, User, GoogleSignIn, FacebookAuth, FirebaseAuth])
void main() {
  
  group('Firebase Authenticator', () {
    const email = 'anyvalidemail';
    const password = 'anyvalidpassword';
    late FirebaseAuth firebaseAuth;
    late FacebookAuth facebookAuth;
    late GoogleSignIn googleSignIn;
    late GoogleAuthenticationProvider googleAuthenticationProvider;
    late FacebookAuthenticationProvider facebookAuthenticationProvider;
    late IAuthenticator firebaseAuthenticator;

    late UserCredential userCredential;
    late User user;

    late SikeUser sikeUser;

    late OAuthCredential authCredential;
    late GoogleSignInAccount googleSignInAccount;
    late GoogleSignInAuthentication googleSignInAuthentication;
    late AccessToken accessToken;

    setUp(() {
      firebaseAuth = MockFirebaseAuth();
      facebookAuth = MockFacebookAuth();
      googleAuthenticationProvider = MockGoogleAuthenticationProvider();
      facebookAuthenticationProvider = MockFacebookAuthenticationProvider();
      googleSignIn = MockGoogleSignIn();
      firebaseAuthenticator = FirebaseAuthenticator(
          firebaseAuth: firebaseAuth,
          googleSignIn: googleSignIn,
          facebookAuth: facebookAuth,
          googleAuthenticationProvider: googleAuthenticationProvider,
          facebookAuthenticationProvider: facebookAuthenticationProvider);
      user = MockUser();
      userCredential = MockUserCredential();
      sikeUser = SikeUser(email: 'test@email.com', id: 'test');
      authCredential = MockOAuthCredential();
      googleSignInAccount = MockGoogleSignInAccount();
      googleSignInAuthentication = MockGoogleSignInAuthentication();
      accessToken = MockAccessToken();
      resetMockitoState();
    });

    group('.getUser()', () {
      test('returns null when no user logged in', () async {
        when(firebaseAuth.currentUser).thenReturn(null);
        final SikeUser? result = firebaseAuthenticator.getUser();

        expect(result, null);
      });

      test('returns null when user email is not verified', () async {
        
        when(firebaseAuth.currentUser).thenReturn(user);
        when(user.emailVerified).thenReturn(false);

        final SikeUser? result = firebaseAuthenticator.getUser();

        expect(result, null);
      });

      test('returns null when user logged in and email is not verified', () async {
        when(user.uid).thenReturn(sikeUser.id);
        when(user.email).thenReturn(sikeUser.email);
        when(firebaseAuth.currentUser).thenReturn(user);
        when(user.emailVerified).thenReturn(false);

        final SikeUser? result = firebaseAuthenticator.getUser();

        expect(result, null);
      });

      test('returns user when user logged in and email is verified', () async {
        when(user.uid).thenReturn(sikeUser.id);
        when(user.email).thenReturn(sikeUser.email);
        when(firebaseAuth.currentUser).thenReturn(user);
        when(user.emailVerified).thenReturn(true);

        final SikeUser? result = firebaseAuthenticator.getUser();

        if(result == null){
          fail('No user returned');
        }

        expect(result.id, sikeUser.id);
        expect(result.email, sikeUser.email);
      });
    });

    group('.isSignedIn()', () {
      test('returns false when no user is signed in', () async {
        when(firebaseAuth.currentUser).thenReturn(null);
        final bool result = firebaseAuthenticator.isSignedIn();

        expect(result, false);
      });

      test('returns false user email is not verified', () async {
        when(user.uid).thenReturn(sikeUser.id);
        when(user.email).thenReturn(sikeUser.email);
        when(user.emailVerified).thenReturn(false);
        when(firebaseAuth.currentUser).thenReturn(user);

        final bool result = firebaseAuthenticator.isSignedIn();
        expect(result, false);
      });

      test('returns true when user email is verified', () async {
        when(user.uid).thenReturn(sikeUser.id);
        when(user.email).thenReturn(sikeUser.email);
        when(user.emailVerified).thenReturn(true);
        when(firebaseAuth.currentUser).thenReturn(user);

        final SikeUser signedInUser = firebaseAuthenticator.getUser()!;
        final bool result = firebaseAuthenticator.isSignedIn();

        expect(signedInUser.id, sikeUser.id);
        expect(signedInUser.email, sikeUser.email);
        expect(result, true);
      });
    });

    group('.sendVerificationLink()', () {
      test('throws if user not signed in', () async {
        expect(Future.value(firebaseAuthenticator.sendVerificationLink()),
            throwsException);
      });

      test('returns true when user is logged in and call successful', () async {
        when(firebaseAuth.currentUser).thenReturn(user);

        final result = await firebaseAuthenticator.sendVerificationLink();

        expect(result, true);
      });
    });

    group('.singInWithCredentials()', () {
      test('rethrows uncaught errors/exceptions', () async {
        when(firebaseAuth.signInWithEmailAndPassword(
                email: email, password: password))
            .thenAnswer((_) => Future.error(Error()));

        expect(
            Future.value(
                firebaseAuthenticator.signInWithCredentials(email, password)),
            throwsA(anything));
      });
      test('returns false when user is null', () async {
        when(userCredential.user).thenReturn(null);
        when(firebaseAuth.signInWithEmailAndPassword(
                email: email, password: password))
            .thenAnswer((_) => Future.value(userCredential));

        final bool result =
            await firebaseAuthenticator.signInWithCredentials(email, password);

        expect(result, false);
      });

      test('throws when user email is not verified', () async {
        when(firebaseAuth.signInWithEmailAndPassword(
                email: email, password: password))
            .thenAnswer((_) => Future.value(userCredential));
        when(userCredential.user).thenReturn(user);
        when(user.emailVerified).thenReturn(false);

        expect(Future.value(
                firebaseAuthenticator.signInWithCredentials(email, password)),
            throwsA(equals(EmailNotVerifiedException)));
      });

      test('returns true when user email is verified', () async {
        when(firebaseAuth.signInWithEmailAndPassword(
                email: email, password: password))
            .thenAnswer((_) => Future.value(userCredential));
        when(userCredential.user).thenReturn(user);
        when(user.emailVerified).thenReturn(true);

        final bool result =
            await firebaseAuthenticator.signInWithCredentials(email, password);
        expect(result, true);
      });
    });

    group('.signUp()', () {
      test('throws uncaught errors/exceptions', () async {
        when(firebaseAuth.createUserWithEmailAndPassword(
                email: email, password: password))
            .thenAnswer((_) => Future.error(Error()));

        expect(Future.value(firebaseAuthenticator.signUp(email, password)),
            throwsA(anything));
      });
      test('returns false when user is null', () async {
        when(userCredential.user).thenReturn(null);
        when(firebaseAuth.createUserWithEmailAndPassword(
                email: email, password: password))
            .thenAnswer((_) => Future.value(userCredential));

        final bool result = await firebaseAuthenticator.signUp(email, password);

        expect(result, false);
      });

      test('returns true when user signed up', () async {
        when(firebaseAuth.createUserWithEmailAndPassword(
                email: email, password: password))
            .thenAnswer((_) => Future.value(userCredential));
        when(userCredential.user).thenReturn(user);

        final bool result = await firebaseAuthenticator.signUp(email, password);
        expect(result, true);
      });
    });

    group('.singInWithGoogle()', () {
      test('throws uncaught errors/exceptions', () async {
        when(googleSignIn.signIn()).thenAnswer((_) => Future.error(Error()));

        expect(Future.value(firebaseAuthenticator.signInWithGoogle()),
            throwsA(anything));
      });

      test('returns false when user is null', () async {
        const String idToken = 'idToken';
        const String accessToken = 'accessToken';

        when(googleSignIn.signIn())
            .thenAnswer((_) => Future.value(googleSignInAccount));
        when(googleSignInAccount.authentication)
            .thenAnswer((_) => Future.value(googleSignInAuthentication));
        when(googleSignInAuthentication.accessToken).thenReturn(accessToken);
        when(googleSignInAuthentication.idToken).thenReturn(idToken);
        when(googleAuthenticationProvider.credential(
                accessToken: accessToken, idToken: idToken))
            .thenReturn(authCredential);
        when(firebaseAuth.signInWithCredential(authCredential))
            .thenAnswer((_) => Future.value(userCredential));
        when(userCredential.user).thenReturn(null);

        final bool result = await firebaseAuthenticator.signInWithGoogle();

        expect(result, false);
      });

      test('returns true when user is signed in (not null)', () async {
        const String idToken = 'idToken';
        const String accessToken = 'accessToken';

        when(googleSignIn.signIn())
            .thenAnswer((_) => Future.value(googleSignInAccount));
        when(googleSignInAccount.authentication)
            .thenAnswer((_) => Future.value(googleSignInAuthentication));
        when(googleSignInAuthentication.accessToken).thenReturn(accessToken);
        when(googleSignInAuthentication.idToken).thenReturn(idToken);
        when(googleAuthenticationProvider.credential(
                accessToken: accessToken, idToken: idToken))
            .thenReturn(authCredential);
        when(firebaseAuth.signInWithCredential(authCredential))
            .thenAnswer((_) => Future.value(userCredential));
        when(userCredential.user).thenReturn(user);

        final bool result = await firebaseAuthenticator.signInWithGoogle();

        expect(result, true);
      });
    });

    group('.singInWithFacebook()', () {
      test('throws uncaught errors/exceptions', () async {
        when(facebookAuth.login()).thenAnswer((_) => Future.error(Error()));

        expect(Future.value(firebaseAuthenticator.signInWithFacebook()),
            throwsA(anything));
      });

      test('facebook cancelled throws correct error message', () async {
        when(facebookAuth.login()).thenAnswer((_) => Future.value(
            LoginResult(status: LoginStatus.cancelled, message: 'anything')));

        expect(
            Future.value(firebaseAuthenticator.signInWithFacebook()),
            throwsA((e) =>
                e is AuthenticationException &&
                e.message == Strings.singInCancelled));
      });

      test('facebook operation in progress throws correct error message',
          () async {
        when(facebookAuth.login()).thenAnswer((_) => Future.value(
            LoginResult(
                status: LoginStatus.operationInProgress, message: 'anything')));

        expect(
            Future.value(firebaseAuthenticator.signInWithFacebook()),
            throwsA((e) =>
                e is AuthenticationException &&
                e.message == Strings.signInInProgress));
      });

      test('facebook sign in failed throws correct error message', () async {
        when(facebookAuth.login()).thenAnswer((_) => Future.value(LoginResult(
                status: LoginStatus.failed, message: 'anything', accessToken: accessToken)));

        expect(
            Future.value(firebaseAuthenticator.signInWithFacebook()),
            throwsA((e) =>
                e is AuthenticationException &&
                e.message == Strings.signInException));
      });

      test('returns false when user is null', () async {
        const String token = 'token';

        when(facebookAuth.login()).thenAnswer((_) => Future.value(LoginResult(
                status: LoginStatus.success, message: 'anything', accessToken: accessToken)));
        when(accessToken.token).thenReturn(token);
        when(facebookAuthenticationProvider.credential(accessToken: token))
            .thenReturn(authCredential);
        when(firebaseAuth.signInWithCredential(authCredential))
            .thenAnswer((_) => Future.value(userCredential));
        when(userCredential.user).thenReturn(null);

        final bool result = await firebaseAuthenticator.signInWithFacebook();

        expect(result, false);
      });

      test('returns true when user is signed in (not null)', () async {
        const String token = 'token';

        when(facebookAuth.login()).thenAnswer((_) => Future.value(LoginResult(
                status: LoginStatus.success, message: 'anything', accessToken: accessToken)));
        when(accessToken.token).thenReturn(token);
        when(facebookAuthenticationProvider.credential(accessToken: token))
            .thenReturn(authCredential);
        when(firebaseAuth.signInWithCredential(authCredential))
            .thenAnswer((_) => Future.value(userCredential));
        when(userCredential.user).thenReturn(user);

        final bool result = await firebaseAuthenticator.signInWithFacebook();

        expect(result, true);
      });
    });
  });
}
