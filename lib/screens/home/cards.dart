import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/models/card.dart';
import 'package:sike/screens/home/card_display.dart';

class Cards extends StatefulWidget {
  Cards({Key? key, ScreenResponseHelper? srh}): srh = srh ?? GetIt.I<ScreenResponseHelper>(),   super(key: key);

  final ScreenResponseHelper srh;

  @override
  _CardsState createState() => _CardsState();
}

class _CardsState extends State<Cards> {
  late List<SikeCard> cards;
  ScreenResponseHelper get srh => widget.srh;

  @override
  void initState() {
    super.initState();
    cards = <SikeCard>[
      SikeCard(
          maxResponseCharacters: 500,
          instructions:
              'Make up your own plot to the movie title that you think other players will choose!',
          description:
              'These movie titiles all belong to very real movies. Make up your own plot and win a point for every player you sike into thinking your plot is the correct one. Then try to pick the real movie plot!',
          id: 'test1',
          imageUrl: '_',
          assetURI: 'assets/images/card_display/movie_bluff.png',
          title: 'Movie Bluff')
    ]; //GetIt.instance<List<SikeCard>>();
  }

  @override
  Widget build(BuildContext context) {
    return  GridView.count(
        primary: false,
        padding: const EdgeInsets.all(20),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        children: List<Widget>.generate(
            cards.length,
            (int index) => CardDisplay(
                  card: cards[index],
                  srh: srh,
                  homeContext: context,
                )),
      );
  }
}
