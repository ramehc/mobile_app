import 'package:flutter/material.dart';

class ThemeColors{
  static const Color backgroundColor = Colors.white;
  static const Color accentColor = Color(0xFF5A73F1);
  static const Color offWhite = Color(0xFFF5F7F9);
}

//Color(0xFF01cbe4);