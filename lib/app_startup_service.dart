import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/services/authentication/firebase_authenticator.dart';
//import 'package:sike/services/user_repository/user_respository.dart';

import 'package:sike/services/local_store/shared_preference_storage_service.dart';
import 'package:sike/services/local_store/storage_service.dart';

Future setupServiceLocator() async{
  final ILocalStorageService localStorageService = await SharedLocalStorageService.getInstance();
  await Firebase.initializeApp();
  final FirebaseStorage storage = FirebaseStorage.instance;
  final ScreenResponseHelper screenResponseHelper = ScreenResponseHelper(411.42857142857144, 797.7142857142857);
  final IAuthenticator authenticator = FirebaseAuthenticator();

  GetIt.I.registerSingleton<FirebaseStorage>(storage);
  GetIt.I.registerSingleton<IAuthenticator>(authenticator);
  GetIt.I.registerSingleton<ScreenResponseHelper>(screenResponseHelper);
  GetIt.I.registerSingleton<ILocalStorageService>(localStorageService);
}