import 'package:flutter_test/flutter_test.dart';
import 'package:sike/services/local_store/shared_preference_storage_service.dart';
import 'package:sike/services/local_store/preference.dart';
import 'package:sike/services/local_store/storage_service.dart';

void main() {
  group('Shared Local Storage Service', () {
    group('.getFromLocalStorage', () {
      test('empty preference returns null', () async {
        final ILocalStorageService localStorageService =
            await SharedLocalStorageService.getInstance() ;
        await localStorageService.clear();

        final dynamic displayName =
            localStorageService.getFromLocalStorage(Preference.displayName);

        expect(displayName, null);
      });

      test('stored preference is returned', () async {
        final ILocalStorageService localStorageService =
            await SharedLocalStorageService.getInstance() ;
        const String displayName = 'Test';
        await localStorageService.clear();

        localStorageService.saveToLocalStorage(
            Preference.displayName, displayName);

        final dynamic retrievedValue =
            localStorageService.getFromLocalStorage(Preference.displayName);

        expect(retrievedValue, displayName);
      });
    });

    group('.valueExist', () {
      test('value does not exist returns false', () async {
        final ILocalStorageService localStorageService =
            await SharedLocalStorageService.getInstance() ;
        await localStorageService.clear();

        final bool result = localStorageService.valueExist(Preference.displayName);

        expect(result, false);
      });

      test('value does not exist returns false', () async {
        final ILocalStorageService localStorageService =
            await SharedLocalStorageService.getInstance() ;
        await localStorageService.clear();

        localStorageService.saveToLocalStorage(Preference.displayName, 'any value');
        final bool result = localStorageService.valueExist(Preference.displayName);

        expect(result, true);
      });

      
    });
  });
}
