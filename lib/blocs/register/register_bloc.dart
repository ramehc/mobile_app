import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';
import 'package:sike/exceptions/error_message_service.dart';
import 'package:sike/helpers/validators.dart';
import 'package:sike/constants/authentication_errors.dart';
import 'package:sike/services/authentication/authenticator.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc({IAuthenticator? authenticator}) :
  _authenticator = authenticator ?? GetIt.I<IAuthenticator>(), super(RegisterState.empty());

  final IAuthenticator _authenticator;

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    switch(event.runtimeType){
      case EmailChanged:
        yield* _mapEmailChangedToState((event as EmailChanged).email);
        break;
      case PasswordChanged:
        yield* _mapPasswordChangedToState((event as PasswordChanged).password);
        break;
      case Submitted:
        yield* _mapFormSubmittedToState((event as Submitted).email, event.password);
        break;
    }
  }

  Stream<RegisterState> _mapEmailChangedToState(String email) async* {
    yield state.update(
      isEmailValid: Validators.isValidEmail(email),
    );
  }

  Stream<RegisterState> _mapPasswordChangedToState(String password) async* {
    yield state.update(
      isPasswordValid: Validators.isValidPassword(password),
    );
  }

  Stream<RegisterState> _mapFormSubmittedToState(
    String email,
    String password,
  ) async* {
    yield RegisterState.loading();
    try {
      if(await _authenticator.signUp(email,password)){
        yield RegisterState.success();
        return;
      }
      yield RegisterState.failure();
    } catch(e){
      if(e is PlatformException){
        switch (e.code) {
          case AuthenticationErrors.emailAlreadyInUse:
            yield state.update(isEmailTaken: true, isEmailValid: false, errorMessage: e.message);
            break;
          case AuthenticationErrors.weakPassword:
            yield state.update(isPasswordValid: false, errorMessage: e.message);
            break;
          case AuthenticationErrors.invalidEmail:
            yield state.update(isEmailValid: false, errorMessage: e.message);
            break;
          default:
            yield RegisterState.failure(errorMessage: ErrorService.getErrorMessage(e));
        }
        return;
      }

      yield RegisterState.failure(errorMessage: ErrorService.getErrorMessage(e));
    }
  }

}
