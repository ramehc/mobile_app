part of 'login_bloc.dart';

@immutable
class LoginState {

  const LoginState({
    required this.isEmailValid,
    required this.isPasswordValid,
    required this.isSubmitting,
    required this.isSuccess,
    required this.isFailure,
    required this.isUserNotFound,
    required this.isUserDisabled,
    required this.isPasswordWrong,
    required this.isTooManyAttempts,
    required this.isEmailNotVerified,
    this.errorMessage
  });

  factory LoginState.empty() {
    return const LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isTooManyAttempts: false,
      isPasswordWrong: false,
      isUserDisabled: false,
      isUserNotFound: false,
      isEmailNotVerified: false,
    );
  }

  factory LoginState.loading() {
    return const LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
      isTooManyAttempts: false,
      isPasswordWrong: false,
      isUserDisabled: false,
      isUserNotFound: false,
      isEmailNotVerified: false
    );
  }

  factory LoginState.failure({String? errorMessage}) {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
      isTooManyAttempts: false,
      isPasswordWrong: false,
      isUserDisabled: false,
      isUserNotFound: false,
      isEmailNotVerified: false,
      errorMessage: errorMessage
    );
  }

  factory LoginState.success() {
    return const LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
      isTooManyAttempts: false,
      isPasswordWrong: false,
      isUserDisabled: false,
      isUserNotFound: false,
      isEmailNotVerified: false
    );
  }

  LoginState update({
    bool? isEmailValid,
    bool? isPasswordValid,
    bool? isPasswordWrong,
    bool? isUserDisabled,
    bool? isTooManyAttempts,
    bool? isUserNotFound,
    bool? isEmailNotVerified,
    String? errorMessage
  }) {
    return copyWith(
      isEmailValid: isEmailValid,
      isPasswordValid: isPasswordValid,
      isPasswordWrong: isPasswordWrong,
      isTooManyAttempts: isTooManyAttempts,
      isUserDisabled: isUserDisabled,
      isUserNotFound: isUserNotFound,
      isEmailNotVerified: isEmailNotVerified,
      errorMessage: errorMessage,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  LoginState copyWith({
    bool? isEmailValid,
    bool? isPasswordValid,
    bool? isSubmitting,
    bool? isSuccess,
    bool? isFailure,
    bool? isPasswordWrong,
    bool? isUserNotFound,
    bool? isUserDisabled,
    bool? isTooManyAttempts,
    bool? isEmailNotVerified,
    String? errorMessage
  }) {
    return LoginState(
      isEmailValid: isEmailValid ?? this.isEmailValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      isPasswordWrong: isPasswordWrong ?? this.isPasswordWrong,
      isUserNotFound: isUserNotFound ?? this.isUserNotFound,
      isUserDisabled: isUserDisabled ?? this.isUserDisabled,
      isTooManyAttempts: isTooManyAttempts ?? this.isTooManyAttempts,
      isEmailNotVerified: isEmailNotVerified ?? this.isEmailNotVerified,
      errorMessage: errorMessage
    );
  }

  @override
  String toString() {
    return '''
    LoginState {
      isEmailValid: $isEmailValid,
      isPasswordValid: $isPasswordValid,
      isWrongPassword: $isPasswordWrong,
      isUserDisabled: $isUserDisabled,
      isTooManyAttempts: $isTooManyAttempts,
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
      isUserNotFound: $isUserNotFound,
      isEmailNotVerified: $isEmailNotVerified,
      errorMessage: $errorMessage
    }''';
  }

  final bool isEmailValid;
  final bool isPasswordValid;
  final bool isPasswordWrong;
  final bool isUserDisabled;
  final bool isTooManyAttempts;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final bool isUserNotFound;
  final bool isEmailNotVerified;
  final String? errorMessage;

  bool get isFormValid => isEmailValid && isPasswordValid;
}
