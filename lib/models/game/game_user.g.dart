// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameUser _$GameUserFromJson(Map<String, dynamic> json) {
  return GameUser(
    id: json['id'] as String,
    displayName: json['displayName'] as String,
    profilePictureUrl: json['profilePictureUrl'] as String,
    sikePictureUrl: json['sikePictureUrl'] as String,
  );
}

Map<String, dynamic> _$GameUserToJson(GameUser instance) => <String, dynamic>{
      'id': instance.id,
      'displayName': instance.displayName,
      'profilePictureUrl': instance.profilePictureUrl,
      'sikePictureUrl': instance.sikePictureUrl,
    };
