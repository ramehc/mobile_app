part of 'setting_bloc.dart';

abstract class SettingEvent extends Equatable {
  const SettingEvent();

  @override
  List<Object?> get props => [];
}

class SettingChanged extends SettingEvent{
  const SettingChanged(this.value);
  final String value;

  @override
  List<Object?> get props => [value];
}

class SettingUpdateError extends SettingEvent{
  const SettingUpdateError(this.errorMessage): assert(errorMessage != '');
  final String errorMessage;

  @override
  List<Object?> get props => [errorMessage];
}

class SettingChanging extends SettingEvent{}

class SettingInitialized extends SettingEvent{}

class SettingUpdating extends SettingEvent{}
