import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sike/constants/authentication_errors.dart';
import 'package:sike/exceptions/error_message_service.dart';
import 'package:sike/exceptions/exceptions.dart';
import 'package:sike/models/user.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/strings.dart';

@visibleForTesting
class FirebaseAuthenticator extends IAuthenticator {
  FirebaseAuthenticator(
      {FirebaseAuth? firebaseAuth,
      GoogleSignIn? googleSignIn,
      FacebookAuth? facebookAuth,
      FacebookAuthenticationProvider? facebookAuthenticationProvider,
      GoogleAuthenticationProvider? googleAuthenticationProvider})
      : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
        _googleSignIn = googleSignIn ?? GoogleSignIn(),
        _facebookAuth = facebookAuth ?? FacebookAuth.instance,
        _facebookAuthenticationProvider = facebookAuthenticationProvider ??
            FacebookAuthenticationProvider.instance,
        _googleAuthenticationProvider = googleAuthenticationProvider ??
            GoogleAuthenticationProvider.instance;

  final FirebaseAuth _firebaseAuth;
  final GoogleSignIn _googleSignIn;
  final FacebookAuth _facebookAuth;
  final GoogleAuthenticationProvider _googleAuthenticationProvider;
  final FacebookAuthenticationProvider _facebookAuthenticationProvider;

  @override
  SikeUser? getUser() {
    final User? currentUser = _firebaseAuth.currentUser;

    if (currentUser == null) {
      return null;
    }

    if(!currentUser.emailVerified){
      return null;
    }

    return SikeUser(email: currentUser.email!, id: currentUser.uid);
  }

  @override
  bool isSignedIn() {
    final User? currentUser = _firebaseAuth.currentUser;

    if (currentUser == null) {
      return false;
    }

    return currentUser.emailVerified;
  }

  @override
  Future<void> sendRestPasswordEmail({required String email}) async {
    //TODO transform exception for old
    try {
      await _firebaseAuth.sendPasswordResetEmail(email: email);
    } catch (e) {
      if(e is FirebaseAuthException){
        _handleFirebaseAuthException(e);
      }

      rethrow;
    }
    
  }

  @override
  Future<bool> sendVerificationLink() async {
    try {
      await Future.delayed(const Duration(seconds: 5));
      final User? user = _firebaseAuth.currentUser;

      if (user == null) {
        throw AuthenticationException(ErrorService.getErrorMessage(null,
            defaultMessage: Strings.errorSendingEmailVerification));
      }

      await user.sendEmailVerification();
      return true;
    } catch (e) {
      throw AuthenticationException(ErrorService.getErrorMessage(e,
          defaultMessage: Strings.errorSendingEmailVerification));
    }
  }

  @override
  Future<bool> signInWithCredentials(String email, String password) async {
    try {
      final UserCredential result =
          await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      if (result.user == null) {
        return false;
      }

      if (!result.user!.emailVerified) {
        throw EmailNotVerifiedException;
      }

      return true;
    } catch (e) {

      if(e is FirebaseAuthException){
        _handleFirebaseAuthException(e);
      }

      rethrow;
    }
  }

void _handleFirebaseAuthException (FirebaseAuthException e){
    switch (e.code) {
      case FirebaseAuthExceptionCodes.wrongPassword:
        throw PlatformException(code: AuthenticationErrors.wrongPassword, message: e.message);
      case FirebaseAuthExceptionCodes.userDisabled:
        throw PlatformException(code: AuthenticationErrors.userDisabled, message: e.message);
      case FirebaseAuthExceptionCodes.invalidEmail:
        throw PlatformException(code: AuthenticationErrors.invalidEmail, message: e.message);
      case FirebaseAuthExceptionCodes.userNotFound:
        throw PlatformException(code: AuthenticationErrors.userNotFound, message: e.message);
      case FirebaseAuthExceptionCodes.tooManyAttempts:
        throw PlatformException(code: AuthenticationErrors.tooManyAttempts, message: e.message);
      case FirebaseAuthExceptionCodes.accountAlreadyExist:
      case FirebaseAuthExceptionCodes.emailAlreadyInUse:
        throw PlatformException(code: AuthenticationErrors.emailAlreadyInUse, message: e.message);
      case FirebaseAuthExceptionCodes.weakPassword:
        throw PlatformException(code: AuthenticationErrors.weakPassword, message: e.message);
    }
  }

  @override
  Future<bool> signInWithFacebook() async {
    try {
      final LoginResult loginResult = await _facebookAuth.login();

      switch (loginResult.status) {
        case LoginStatus.success:
          final bool isSignedIn =
              await _firebaseAuthWithFacebook(loginResult.accessToken!.token);
          return isSignedIn;
        case LoginStatus.operationInProgress:
          throw AuthenticationException(ErrorService.getErrorMessage(null,
              defaultMessage: Strings.signInInProgress));
        case LoginStatus.cancelled:
          throw AuthenticationException(ErrorService.getErrorMessage(null,
              defaultMessage: Strings.singInCancelled));
        case LoginStatus.failed:
          throw AuthenticationException(ErrorService.getErrorMessage(null,
              defaultMessage: Strings.signInException));
      }

    } on Exception catch (e) {
      throw AuthenticationException(ErrorService.getErrorMessage(e,
          defaultMessage: Strings.signInException));
    }
  }

  Future<bool> _firebaseAuthWithFacebook(String token) async {
    try {
      final OAuthCredential credential =
          _facebookAuthenticationProvider.credential(accessToken: token);
      final UserCredential authResult =
          await _firebaseAuth.signInWithCredential(credential);
      return authResult.user != null;
    } catch (e) {
      throw AuthenticationException(ErrorService.getErrorMessage(e,
          defaultMessage: Strings.signInException));
    }
  }

  @override
  Future<bool> signInWithGoogle() async {
    try {
      final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();
      if (googleUser == null) {
        throw AuthenticationException(ErrorService.getErrorMessage(null,
            defaultMessage: Strings.signInException));
      }

      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final OAuthCredential credential =
          _googleAuthenticationProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      final UserCredential result =
          await _firebaseAuth.signInWithCredential(credential);
      return result.user != null;
    } catch (e) {
      if (e is AuthenticationException) {
        rethrow;
      }

      throw AuthenticationException(ErrorService.getErrorMessage(e,
          defaultMessage: Strings.signInException));
    }
  }

  @override
  Future<bool> signOut() async {
    //TODO check if also accounts for facebook signout
    //TODO check if only firebase auth is needed for signout
    try {
      Future.wait(<Future<dynamic>>[
        _firebaseAuth.signOut(),
        _googleSignIn.signOut(),
      ]);
      return true;
    } catch (e) {
      throw AuthenticationException(ErrorService.getErrorMessage(e,
          defaultMessage: Strings.signInException));
    }
  }

  @override
  Future<bool> signUp(String email, String password) async {
    try {
      final UserCredential result =
          await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      if (result.user == null) {
        return false;
      }

      await result.user!.sendEmailVerification();
      return true;
    } catch (e) {
      if(e is FirebaseAuthException){
        _handleFirebaseAuthException(e);
      }
      
      throw AuthenticationException(ErrorService.getErrorMessage(e,
          defaultMessage: Strings.signInException));
    }
  }
}

class GoogleAuthenticationProvider {
  GoogleAuthenticationProvider._internal();
  static final GoogleAuthenticationProvider _instance =
      GoogleAuthenticationProvider._internal();

  static GoogleAuthenticationProvider get instance => _instance;

  OAuthCredential credential({String? idToken, String? accessToken}) =>
      GoogleAuthProvider.credential(idToken: idToken, accessToken: accessToken);
}

class FacebookAuthenticationProvider {
  FacebookAuthenticationProvider._internal();
  static final FacebookAuthenticationProvider _instance =
      FacebookAuthenticationProvider._internal();

  static FacebookAuthenticationProvider get instance => _instance;

  OAuthCredential credential({required String accessToken}) =>
      FacebookAuthProvider.credential(accessToken);
}

abstract class FirebaseAuthExceptionCodes {
  static const String wrongPassword = 'wrong-password';
  static const String userDisabled = 'user-disabled';
  static const String invalidEmail = 'invalid-email';
  static const String userNotFound = 'user-not-found';
  static const String tooManyAttempts = 'operation-not-allowed';
  static const String accountAlreadyExist = 'account-exists-with-different-credential';
  static const String emailAlreadyInUse = 'email-already-in-use';
  static const String weakPassword = 'weak=password';
  static const String operationNotAllowed = 'operation-not-allowed';
}
