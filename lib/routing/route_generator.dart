import 'package:flutter/material.dart';
import 'package:sike/screens/settings/profile.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/splash.dart';
import 'package:sike/constants/routes.dart';
import 'package:sike/screens/login/login.dart';
import 'package:sike/screens/login/reset_password.dart';
import 'package:sike/screens/register/register.dart';

class RouteGenerator{
  static Route<dynamic> generateRoute(RouteSettings settings){
    final Object? args = settings.arguments;

    switch (settings.name) {
      case Routes.splash:
        return MaterialPageRoute<SplashScreen>(builder: (_) => SplashScreen());
      case Routes.login:
        return MaterialPageRoute<LoginScreen>(builder: (_) => LoginScreen());
      case Routes.resetPassword:
        return MaterialPageRoute<ResetPassword>(builder: (_) =>  ResetPassword());
      case Routes.register:
        return MaterialPageRoute<Register>(builder: (_) =>  Register());
      case Routes.profile:
        return MaterialPageRoute<Profile>(builder: (_) => Profile());
      default:{
        return _errorRoute();
      }
    }

  }

  static Route<dynamic> _errorRoute(){
    return MaterialPageRoute<dynamic>(builder: (_){
      return Scaffold(
        body: Center(
          child: SikeText('ERROR'),
        ),
      );

    });
  }
}