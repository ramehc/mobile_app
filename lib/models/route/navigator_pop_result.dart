import 'package:sike/models/route/pop_result.dart';

class NavigatorPopResult{
  NavigatorPopResult({required this.displayMessage, required this.result});

  final String displayMessage;
  final PopResult result;
}