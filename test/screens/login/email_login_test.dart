import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/login/login_bloc.dart';
import 'package:sike/widgets/resend_verification_button.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/constants/authentication_errors.dart';
import 'package:sike/exceptions/exceptions.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/screens/login/email_login.dart';
import 'package:sike/strings.dart';
import './email_login_test.mocks.dart';

@GenerateMocks([IAuthenticator])
void main() {
  LiveTestWidgetsFlutterBinding();
  late LoginBloc loginBloc;
  late IAuthenticator authenticator;
  final ScreenResponseHelper screenResponseHelper =
      ScreenResponseHelper(411.42857142857144, 797.7142857142857);
  late EmailLogin emailLogin;
  const String email = 'anyvalidemail@gmail.com';
  const String password = 'AnyValidPassword1';
  final Finder signInButton = find.byType(ElevatedButton);

  setUp(() {
    authenticator = MockIAuthenticator();
    loginBloc = LoginBloc(authenticator: authenticator);
    GetIt.I.registerSingleton<ScreenResponseHelper>(screenResponseHelper);
    emailLogin = EmailLogin(
      loginBloc: loginBloc,
      srh: screenResponseHelper,
    );
  });

  tearDown(() {
    GetIt.I.unregister<ScreenResponseHelper>();
    loginBloc.close();
  });

  Widget makeWidgetTestable(Widget? child) {
    return MaterialApp(
      home: Scaffold(body: child),
    );
  }

  

  Future<void> setEmailField(WidgetTester tester, String email) async{
    final Finder emailField = find.byType(TextFormField).first;

    await tester.tap(emailField);
    await tester.enterText(emailField, email);

    await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));
  }

  Future<void> setPasswordField(WidgetTester tester, String password) async {
    final Finder passwordField = find.byType(TextFormField).last;

    await tester.tap(passwordField);
    await tester.enterText(passwordField, password);

    await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));

  }
  
  Future<void> setEmailAndPasswordField(WidgetTester tester) async {
    await setEmailField(tester, email);
    await setPasswordField(tester, password);
  }

  group('Email Login Widget', () {
    group(
        'Sign in button is only enabled when email and password are valid',
        (){

          testWidgets('Sign in button disabled for invalid email and valid password',  (WidgetTester tester) async{
              await tester.pumpWidget(makeWidgetTestable(emailLogin));

              expect(signInButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(signInButton).enabled, isFalse);

              await setEmailField(tester, 'anyinvalidemail');
              await setPasswordField(tester, password);

              await expectLater(tester.widget<ElevatedButton>(signInButton).enabled,
                  isFalse);
          });

           testWidgets('Sign in button disabled for valid email and invalid password',  (WidgetTester tester) async{
             await tester.pumpWidget(makeWidgetTestable(emailLogin));

              expect(signInButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(signInButton).enabled, isFalse);

              await setEmailField(tester, email);
              await setPasswordField(tester, 'anyinvalidpassword');

              await expectLater(tester.widget<ElevatedButton>(signInButton).enabled,
                  isFalse);
          });

           testWidgets('Sign in button enabled for valid email and valid password',  (WidgetTester tester) async{
             await tester.pumpWidget(makeWidgetTestable(emailLogin));


              expect(signInButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(signInButton).enabled, isFalse);

              await setEmailAndPasswordField(tester);

              await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));

              await expectLater(tester.widget<ElevatedButton>(signInButton).enabled,
                  isTrue);
          });
    });

    testWidgets('Sign in with email and password is called when sign in button is pressed',  (WidgetTester tester) async{
             await tester.pumpWidget(makeWidgetTestable(emailLogin));

              expect(signInButton, findsOneWidget);

              expect(tester.widget<ElevatedButton>(signInButton).enabled, isFalse);

              await setEmailAndPasswordField(tester);

              await tester.pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));

              await expectLater(tester.widget<ElevatedButton>(signInButton).enabled,
                  isTrue);
              
              when(authenticator.signInWithCredentials(email, password)).thenAnswer((_) => Future.value(true));

              await tester.tap(signInButton);

              verify(authenticator.signInWithCredentials(email, password)).called(1);
          });

    group('Correct error message is shown to match email login error state', (){
      testWidgets(
        'Too many attempts',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(emailLogin));
        await setEmailAndPasswordField(tester);
        when(authenticator.signInWithCredentials(email, password)).thenThrow(PlatformException(code: AuthenticationErrors.tooManyAttempts));

        await tester.tap(signInButton);
        await tester.pumpAndSettle(const Duration(milliseconds: 10));

        await expectLater(find.text(Strings.tooManySignIn), findsOneWidget);
      });

      testWidgets(
        'User disabled',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(emailLogin));
        await setEmailAndPasswordField(tester);
        when(authenticator.signInWithCredentials(email, password)).thenThrow(PlatformException(code: AuthenticationErrors.userDisabled));

        await tester.tap(signInButton);
        await tester.pumpAndSettle(const Duration(milliseconds: 10));

        await expectLater(find.text(Strings.accountDisabled), findsOneWidget);
      });

      testWidgets(
        'User not found',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(emailLogin));
        await setEmailAndPasswordField(tester);
        when(authenticator.signInWithCredentials(email, password)).thenThrow(PlatformException(code: AuthenticationErrors.userNotFound));

        await tester.tap(signInButton);
        await tester.pumpAndSettle(const Duration(milliseconds: 10));

        await expectLater(find.text(Strings.userNotFound), findsOneWidget);
      });

      testWidgets(
        'Wrong password',
        (WidgetTester tester) async {

        await tester.pumpWidget(makeWidgetTestable(emailLogin));
        await setEmailAndPasswordField(tester);
        when(authenticator.signInWithCredentials(email, password)).thenThrow(PlatformException(code: AuthenticationErrors.wrongPassword));

        await tester.tap(signInButton);
        await tester.pumpAndSettle(const Duration(milliseconds: 10));

        await expectLater(find.text(Strings.incorrectPassword), findsOneWidget);
      });

      testWidgets(
        'Email not verified',
        (WidgetTester tester) async {

        GetIt.instance.registerSingleton<IAuthenticator>(authenticator);
        await tester.pumpWidget(makeWidgetTestable(emailLogin));
        await setEmailAndPasswordField(tester);
        when(authenticator.signInWithCredentials(email, password)).thenThrow(EmailNotVerifiedException);

        await tester.tap(signInButton);
        await tester.pumpAndSettle(const Duration(milliseconds: 10));

        await expectLater(find.text(Strings.emailNotVerified), findsOneWidget);
        GetIt.instance.unregister<IAuthenticator>();
      });
    });
  });

  testWidgets(
    'Email not verified gives resend verification email link',
    (WidgetTester tester) async {

    GetIt.instance.registerSingleton<IAuthenticator>(authenticator);
    await tester.pumpWidget(makeWidgetTestable(emailLogin));
    await setEmailAndPasswordField(tester);
    when(authenticator.signInWithCredentials(email, password)).thenThrow(EmailNotVerifiedException);

    await tester.tap(signInButton);
    await tester.pumpAndSettle(const Duration(milliseconds: 10));

    await expectLater(find.byType(ResendVerificationLink), findsOneWidget);
    GetIt.instance.unregister<IAuthenticator>();
  });
}
