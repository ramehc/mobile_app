import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/exceptions/error_message_service.dart';
import 'package:sike/helpers/icon/system_icons.dart';
import 'package:sike/models/card.dart';
import 'package:sike/strings.dart';
import 'package:sike/theme/font_size.dart';

class CardDisplay extends StatelessWidget {
  const CardDisplay({Key? key, required this.card, required this.srh, required this.homeContext}) : super(key: key);
  final SikeCard card;
  final ScreenResponseHelper srh;
  final BuildContext homeContext;

  @override
  Widget build(BuildContext context) {
    final double height = srh.getPercentageHeight(0.25);
    return GestureDetector(
      onTap: () => _showCardDialog(context),
      child: Card(
        elevation: 8,
        child: Stack(
          children: <Widget>[
            Positioned.fill(child: _cardImage(height)),
            _title(height)
          ],
        ),
      ),
    );
  }

  void _showCardDialog(BuildContext cardContext) {
    showDialog(
        context: cardContext,
        //barrierColor: null,
        builder: (BuildContext context) {
          return Dialog(
            child: SizedBox(
              height: srh.getPercentageHeight(0.6),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(srh.getRelativeHeight(10)),
                      child: Center(
                          child: Text(
                        card.title,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: srh.getRelativeFontSize(FontSize.majorTitle)),
                      )),
                    ),
                    Container(
                      color: Colors.blue,
                      height: srh.getPercentageHeight(0.2),
                      child: _cardImage(srh.getPercentageHeight(0.2)),
                    ),
                    Expanded(
                      child: ListView(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(srh.getRelativeHeight(10)),
                            child: Center(child: SikeText(Strings.description)),
                          ),
                          Padding(
                            padding: EdgeInsets.all(srh.getRelativeHeight(10)),
                            child: Center(child: Text(card.description, style: TextStyle(fontSize: srh.getRelativeFontSize(FontSize.bodyText)),)),
                          )
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ElevatedButton(
                          onPressed: ()async {
                            try {
                              //final GameUser user = UserSettings.instance.getGameUser();
                              //Navigator.of(context).pop();
                              throw Exception('Testing how errors display');
                              //Navigator.of(cardContext).pushNamed(Pages.GameView, arguments: GameViewModel(user, card));
                            } catch (e) {
                              ScaffoldMessenger.of(cardContext)..hideCurrentSnackBar()..showSnackBar(Toast.error(ErrorService.getErrorMessage(e)));
                            }
                            
                          },
                          child: SikeText(
                            Strings.playOnline,
                            fontColor: Colors.white
                          ),
                        ),
                        ElevatedButton(
                          onPressed: null,
                          child: SikeText(
                            Strings.startGame,
                            fontColor: Colors.white
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  Positioned _title(double height) {
    return Positioned(
      child: Center(
        child: Container(
            constraints: BoxConstraints(
              maxWidth: height * 0.75
            ),
            margin: EdgeInsets.only(
                left: srh.getRelativeWidth(10),
                right: srh.getRelativeWidth(10)),
            padding: EdgeInsets.all(srh.getRelativeWidth(5)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.grey[500]),
            child: Text(
              card.title,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: srh.getRelativeFontSize(FontSize.itemTitle)),
            )),
      ),
    );
  }

  Widget _cardImage(double height) {
    if(card.assetURI != null){
      return Container(
            height: height,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.contain, image: AssetImage(card.assetURI!))),
            child: Container());
    }

    
    return  CachedNetworkImage(
            imageUrl: card.imageUrl,
            imageBuilder: (BuildContext context, ImageProvider imageProvider) => Container(
              decoration: BoxDecoration(
                  image:
                      DecorationImage(image: imageProvider, fit: BoxFit.contain)),
            ),
            placeholder: (BuildContext context, String url) => SizedBox(
              height: height,
              child: const Center(child: CircularProgressIndicator()),
            ),
            errorWidget: (BuildContext context, String url, dynamic error) =>  Container(
              height: height,
              decoration: const BoxDecoration(),
              child: Center(child: Icon(SystemIcon.error)),
            ),
          );
  }
}
