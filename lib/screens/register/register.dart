import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/blocs/register/register_bloc.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/sike_text_form.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/helpers/icon/system_icons.dart';
import 'package:sike/strings.dart';
import 'package:sike/theme/font_size.dart';

class Register extends StatefulWidget {
  Register(
      {Key? key,
      ScreenResponseHelper? srh,
      AuthenticationBloc? authenticationBloc,
      RegisterBloc? registerBloc})
      : _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
        _registerBloc = registerBloc,
        _authenticationBloc =
            authenticationBloc ?? GetIt.I<AuthenticationBloc>(),
        super(key: key);

  final ScreenResponseHelper _srh;
  final RegisterBloc? _registerBloc;
  final AuthenticationBloc _authenticationBloc;

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  ScreenResponseHelper get _srh => widget._srh;
  late RegisterBloc _registerBloc;
  AuthenticationBloc get _authenticationBloc => widget._authenticationBloc;
  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _registerBloc = widget._registerBloc ?? RegisterBloc();
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  void _onEmailChanged() {
    _registerBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _registerBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _registerBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text(Strings.signUp),
        ),
        body: BlocListener<AuthenticationBloc, AuthenticationState>(
          bloc: _authenticationBloc,
          listener: (BuildContext context, AuthenticationState state) {
            if (state is AuthenticationError) {
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(Toast.error(state.errorMessage));
            }
          },
          child: BlocListener<RegisterBloc, RegisterState>(
            bloc: _registerBloc,
            listener: (BuildContext context, RegisterState state) {
              if (state.isSubmitting) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(Toast.loading(Strings.signingUp));
              }

              if(state.errorMessage != null){
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(Toast.error(state.errorMessage!));
              }
            },
            child: BlocBuilder<RegisterBloc, RegisterState>(
              bloc: _registerBloc,
              builder: (BuildContext context, RegisterState state) {
                
                if (state.isSuccess) {
                  return _buildSignUpSuccess(context);
                }
                return _buildSignUpForm(state);
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSignUpSuccess(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: _srh.getRelativeHeight(20)),
        child: Padding(
          padding: EdgeInsets.all(_srh.getRelativeWidth(20)),
          child: Center(
            child: ListView(
              children: <Widget>[
                SikeText(Strings.signUpThanks),
                Container(
                  height: _srh.getRelativeHeight(20),
                ),
                SikeText('${Strings.verificationSent} ${_emailController.text}' ),
                Padding(
                  padding: EdgeInsets.only(top: _srh.getRelativeHeight(50)),
                  child: ElevatedButton.icon(
                      icon: Icon(
                        SystemIcon.back,
                        color: Colors.white,
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        onPrimary: Colors.white,
                        primary: Colors.indigoAccent,
                      ),
                      onPressed: () => Navigator.of(context).pop(),
                      label: SikeText(Strings.goToSignIn)),
                )
              ],
            ),
          ),
        ));
  }

  Widget _buildSignUpForm(RegisterState state) {
    return Padding(
      padding: EdgeInsets.all(_srh.getRelativeWidth(20)),
      child: Form(
        child: Column(
          children: <Widget>[
            SikeTextFormField(
              controller: _emailController,
              icon: SystemIcon.email,
              labelText: Strings.email,
              autoCorrect: false,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (_) {
                return !state.isEmailValid ? Strings.invalidEmail : null;
              },
            ),
            SikeTextFormField(
              controller: _passwordController,
              labelText: Strings.password,
              icon: SystemIcon.lock,
              obscureText: true,
              autoCorrect: false,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (_) {
                return !state.isPasswordValid ? Strings.invalidPassword : null;
              },
            ),
            if(state.isEmailTaken)
            GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Padding(
                padding: EdgeInsets.only(top: _srh.getRelativeHeight(20)),
                child: Text(
                  Strings.emailTaken,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: _srh.getRelativeFontSize(FontSize.bodyText),
                      decoration: TextDecoration.underline),
                ),
              ),
            ),
            Container(
                margin: EdgeInsets.only(top: _srh.getRelativeHeight(10)),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),),
                  onPressed:
                      _isSignUpButtonEnabled(state) ? _onFormSubmitted : null,
                  child: SikeText(Strings.signUp
                ))),
          ],
        ),
      ),
    );
  }

  bool _isSignUpButtonEnabled(RegisterState state) {
    return state.isFormValid && !state.isSubmitting && isPopulated;
  }

  void _onFormSubmitted() {
    _registerBloc.add(
      Submitted(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }
}
