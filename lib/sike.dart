import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/splash.dart';
import 'package:sike/routing/route_generator.dart';
import 'package:sike/screens/home/home.dart';
import 'package:sike/screens/login/login.dart';
import 'package:sike/services/user_settings/settings_service.dart';
import 'package:sike/theme/main_theme.dart';

class Sike extends StatefulWidget {
  Sike(
      {Key? key,
      ScreenResponseHelper? screenResponseHelper,
      AuthenticationBloc? authenticationBloc}):
        _screenResponseHelper =
            screenResponseHelper ?? GetIt.I<ScreenResponseHelper>(),
        _authenticationBloc = authenticationBloc,
        super(key: key);
  final ScreenResponseHelper _screenResponseHelper;
  final AuthenticationBloc? _authenticationBloc;

  @override
  _SikeState createState() => _SikeState();
}

class _SikeState extends State<Sike> {
  ScreenResponseHelper get _screenResponseHelper =>
      widget._screenResponseHelper;
  static late AuthenticationBloc _authenticationBloc;
  late UserSettingsService _userSettingsService;

  @override
  void initState() {
    super.initState();
    _authenticationBloc = widget._authenticationBloc ??
        AuthenticationBloc();
    GetIt.I.registerSingleton<AuthenticationBloc>(_authenticationBloc);
    _userSettingsService = UserSettingsService(authenticationBloc: _authenticationBloc);
    GetIt.I.registerSingleton<UserSettingsService>(_userSettingsService); 
    _authenticationBloc.add(AppStarted());
  }

  @override
  void dispose() {
    GetIt.I.unregister<UserSettingsService>();
    GetIt.I.unregister<AuthenticationBloc>();
    _authenticationBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: defaultTheme,
      locale: DevicePreview.locale(context),
      builder: DevicePreview.appBuilder,
      onGenerateRoute: RouteGenerator.generateRoute,
      home:  BlocBuilder<AuthenticationBloc, AuthenticationState>(
              bloc: _authenticationBloc,
              builder: (BuildContext context, AuthenticationState state) {
                _screenResponseHelper.setContext(context);
                switch (state.runtimeType) {
                  case Uninitialized:
                    return SplashScreen();
                  case Unauthenticated:
                    return LoginScreen();
                  case Authenticated:
                    return Home();
                  default:
                    return const CircularProgressIndicator();
                }
              })
    );
  }
}
