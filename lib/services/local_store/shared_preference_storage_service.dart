import 'package:shared_preferences/shared_preferences.dart';
import 'package:sike/services/local_store/preference.dart';
import 'package:sike/services/local_store/storage_service.dart';

class SharedLocalStorageService extends ILocalStorageService{
  static SharedLocalStorageService? _instance;
  static SharedPreferences? _preferences;

  static Future<ILocalStorageService> getInstance() async {
    _instance ??= SharedLocalStorageService();
    _preferences ??= await SharedPreferences.getInstance();
    return _instance!;
  }

  @override
  dynamic getFromLocalStorage (Preference preference){

    switch (preference) {
      default:
      return _preferences!.getString(preference.toString());
    }

  }

  @override
  void saveToLocalStorage(Preference key, dynamic value){
    _saveToDisk(key.toString(), value);
  }

  @override
  Future<bool> removeFromLocalStorage(Preference key) async{
    return _preferences!.remove(key.toString());
  }

  void _saveToDisk<T>(String key, T content) {
    if (content is String) {
      _preferences!.setString(key, content);
    }
    if (content is bool) {
      _preferences!.setBool(key, content);
    }
    if (content is int) {
      _preferences!.setInt(key, content);
    }
    if (content is double) {
      _preferences!.setDouble(key, content);
    }
    if (content is List<String>) {
      _preferences!.setStringList(key, content);
    }
  }

  @override
  bool valueExist(Preference key){
    return _preferences!.containsKey(key.toString());
  }

  @override
  Future<bool> clear(){
    return _preferences!.clear();
  }

}