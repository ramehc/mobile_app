import 'package:flutter/material.dart';
import 'package:sike/theme/colors.dart';
import 'package:sike/theme/fonts.dart';

ThemeData defaultTheme = ThemeData(
    fontFamily: Fonts.muli,
    backgroundColor: ThemeColors.backgroundColor,
    scaffoldBackgroundColor: ThemeColors.backgroundColor,
    primaryColor: ThemeColors.backgroundColor,
    iconTheme: const IconThemeData(color: Colors.pink),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
      primary: Colors.indigoAccent,
      onPrimary: Colors.white,
      padding: const EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
    )));
