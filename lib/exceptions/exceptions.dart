mixin EmailNotVerifiedException implements Exception {}

class UserError{
  UserError(this.errorMessage);
  final String errorMessage;
}

class SikeException implements Exception {
  final String? message;

  SikeException([this.message]);

  @override
  String toString() {
    final String? message = this.message;
    if (message == null) return "Exception";
    return "Exception: $message";
  }

}

class AuthenticationException extends SikeException{
  AuthenticationException(String message): super(message);
}