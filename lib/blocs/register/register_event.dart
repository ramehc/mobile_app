part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => [];
}

class EmailChanged extends RegisterEvent {
  const EmailChanged({required this.email});
  final String email;

  @override
  List<Object> get props => <Object>[email];
}

class PasswordChanged extends RegisterEvent {

  const PasswordChanged({required this.password});
  final String password;
  @override
  List<Object> get props => <Object>[password];
}

class Submitted extends RegisterEvent {

  const Submitted({required this.email, required this.password});
  final String email;
  final String password;
  @override
  List<Object> get props => <Object>[email, password];
}
