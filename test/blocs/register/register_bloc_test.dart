import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/register/register_bloc.dart';
import 'package:sike/constants/authentication_errors.dart';
import 'package:sike/services/authentication/authenticator.dart';

import 'register_bloc_test.mocks.dart';

@GenerateMocks([IAuthenticator])
void main() {
  group('Register Bloc', () {
    late IAuthenticator authenticator;
    late RegisterBloc registerBloc;
    const String invalidEmail = 'anyInvalidEmail';
    const String validEmail = 'anyvalidemail@gmail.com';
    const String invalidPassword = 'anyinvalidpassword';
    const String validPassword = 'Password1';

    setUp(() {
      authenticator = MockIAuthenticator();
      registerBloc = RegisterBloc(authenticator: authenticator);
    });

    tearDown(() {
      Future.sync(() => registerBloc.close());
    });

    test('Bloc intializes with state empty', () {
      expect(registerBloc.state, RegisterState.empty());
    });

    group('Email Changed Event', () {
      blocTest(
        'When email address is invalid, isEmailValid is false',
        build: () => registerBloc,
        act: (dynamic bloc) =>
            bloc.add(const EmailChanged(email: invalidEmail)),
        expect: () => [
          isA<RegisterState>()
              .having((state) => state.isEmailValid, 'email is valid', isFalse)
        ],
      );

      blocTest(
        'When email address is valid, isEmailValid is true',
        build: () => registerBloc,
        act: (dynamic bloc) => bloc.add(const EmailChanged(email: validEmail)),
        expect: () => [
          isA<RegisterState>()
              .having((state) => state.isEmailValid, 'email is valid', isTrue)
        ],
      );
    });

    group('Password Changed Event', () {
      blocTest(
        'When password is invalid, isPasswordValid is false',
        build: () => registerBloc,
        act: (dynamic bloc) =>
            bloc.add(const PasswordChanged(password: invalidPassword)),
        expect: () => [
          isA<RegisterState>().having(
              (state) => state.isPasswordValid, 'password is valid', isFalse)
        ],
      );

      blocTest(
        'When password is valid, isPasswordValid is true',
        build: () => registerBloc,
        act: (dynamic bloc) =>
            bloc.add(const PasswordChanged(password: validPassword)),
        expect: () => [
          isA<RegisterState>().having(
              (state) => state.isPasswordValid, 'password is valid', isTrue)
        ],
      );
    });

    group('Submit Event', () {
      group('Exceptions', () {
        blocTest(
          'State reflects email already in use exception',
          build: () {
            when(authenticator.signUp(validEmail, validPassword)).thenAnswer(
                (_) => Future.error(PlatformException(
                    code: AuthenticationErrors.emailAlreadyInUse)));
            return registerBloc;
          },
          act: (dynamic bloc) => bloc
              .add(const Submitted(email: validEmail, password: validPassword)),
          expect: () => [
            RegisterState.loading(),
            isA<RegisterState>()
                .having(
                    (state) => state.isEmailValid, 'email is valid', isFalse)
                .having((state) => state.isEmailTaken, 'email is taken', isTrue)
          ],
        );

        blocTest(
          'State reflects weak password exception',
          build: () {
            when(authenticator.signUp(validEmail, validPassword)).thenAnswer(
                (_) => Future.error(PlatformException(
                    code: AuthenticationErrors.weakPassword)));
            return registerBloc;
          },
          act: (dynamic bloc) => bloc
              .add(const Submitted(email: validEmail, password: validPassword)),
          expect: () => [
            RegisterState.loading(),
            isA<RegisterState>()
                .having(
                    (state) => state.isPasswordValid, 'password is valid', isFalse)
          ],
        );

        blocTest(
          'State reflects invalid email exception',
          build: () {
            when(authenticator.signUp(validEmail, validPassword)).thenAnswer(
                (_) => Future.error(PlatformException(
                    code: AuthenticationErrors.invalidEmail)));
            return registerBloc;
          },
          act: (dynamic bloc) => bloc
              .add(const Submitted(email: validEmail, password: validPassword)),
          expect: () => [
            RegisterState.loading(),
            isA<RegisterState>()
                .having(
                    (state) => state.isEmailValid, 'email is valid', isFalse)
          ],
        );

        blocTest(
          'Random error yield loading, then failure state',
          build: () {
            when(authenticator.signUp(validEmail, validPassword)).thenAnswer(
                (_) => Future.error(Error()));
            return registerBloc;
          },
          act: (dynamic bloc) => bloc
              .add(const Submitted(email: validEmail, password: validPassword)),
          expect: () => [
            RegisterState.loading(),
            isA<RegisterState>().having((state) => state.isFailure, 'isFailure', isTrue)
          ],
        );
      });

      blocTest(
        'Unsuccessful sign up emits loading, then failure',
        build: () {
            when(authenticator.signUp(validEmail, validPassword)).thenAnswer(
                (_) => Future.value(false));
            return registerBloc;
          },
        act: (dynamic bloc) =>
            bloc.add(const Submitted(email: validEmail, password: validPassword)),
        expect: () => [RegisterState.loading(), isA<RegisterState>().having((state) => state.isFailure, 'isFailure', isTrue)],
      );

      blocTest(
        'Successful sign up emits loading, then success',
        build: () {
            when(authenticator.signUp(validEmail, validPassword)).thenAnswer(
                (_) => Future.value(true));
            return registerBloc;
          },
        act: (dynamic bloc) =>
            bloc.add(const Submitted(email: validEmail, password: validPassword)),
        expect: () => [RegisterState.loading(), RegisterState.success()],
      );
    });
  });
}
