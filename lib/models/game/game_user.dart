import 'package:json_annotation/json_annotation.dart';

part 'game_user.g.dart';

@JsonSerializable()
class GameUser {GameUser(
      {required this.id, required this.displayName, required this.profilePictureUrl, required this.sikePictureUrl});

  factory GameUser.fromJson(Map<String, dynamic> json) => _$GameUserFromJson(json);
  
  String id;
  String displayName = '';
  String profilePictureUrl = '';
  String sikePictureUrl = '';

  Map<String, dynamic> toJson() => _$GameUserToJson(this);

}
