class FontSize{
  static const double majorTitle = 24;
  static const double itemTitle = 16;
  static const double minorTitle = 14;
  static const double bodyText = 14;
  static const double iconSize = 24;
  static const double buttonText = 14;
}