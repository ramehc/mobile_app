import 'package:flutter/services.dart';
import 'package:sike/exceptions/exceptions.dart';

class ErrorService{

  static const _genericErrorMessage = 'Oops! Something went wrong!';

  static String getErrorMessage(dynamic error,{String? defaultMessage}){
    if(error is SikeException){
      return error.message ?? _genericErrorMessage;
    }

    if(error is AuthenticationException){
      return error.message ?? _genericErrorMessage;
    }
    
    if(error is PlatformException){
      switch (error.code) {
        case 'ERROR_NETWORK_REQUEST_FAILED':
          return 'Request failed due to network issues';
        case 'sign_in_failed':{
          if(error.message!.contains('ApiException: 7')){
            return'Request failed due to network issues';
          }
        }
          break;
        default:
          return error.message ?? _genericErrorMessage;
      }
    }
    
    return defaultMessage ?? _genericErrorMessage;
  }

}