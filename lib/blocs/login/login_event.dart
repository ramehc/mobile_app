
part of 'login_bloc.dart';

@immutable
abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class EmailChanged extends LoginEvent {
  const EmailChanged({required this.email});
  final String email;

  @override
  List<Object> get props => <Object>[email];
}

class PasswordChanged extends LoginEvent {
  const PasswordChanged({required this.password});
  final String password;

  @override
  List<Object> get props => <Object>[password];
}

class LoginWithGoogle extends LoginEvent {
  @override
  List<Object> get props => [];

}

class LoginWithFacebook extends LoginEvent{
  @override
  List<Object> get props => [];
}

class LoginWithCredentials extends LoginEvent {

  const LoginWithCredentials({required this.email, required this.password});

  final String email;
  final String password;

  @override
  List<Object> get props => <Object>[email, password];

}