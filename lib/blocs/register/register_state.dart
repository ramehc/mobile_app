part of 'register_bloc.dart';

@immutable
class RegisterState {
  const RegisterState({
    required this.isEmailValid,
    required this.isPasswordValid,
    required this.isSubmitting,
    required this.isSuccess,
    required this.isFailure,
    required this.isEmailTaken,
    this.errorMessage
  });

  factory RegisterState.empty() {
    return const RegisterState(
      isEmailValid: true,
      isEmailTaken: false,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  factory RegisterState.loading() {
    return const RegisterState(
      isEmailValid: true,
      isEmailTaken: false,
      isPasswordValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
    );
  }


  factory RegisterState.success() {
    return const RegisterState(
      isEmailValid: true,
      isEmailTaken: false,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
    );
  }



  factory RegisterState.failure({String? errorMessage}) {
    return RegisterState(
      isEmailValid: true,
      isEmailTaken: false,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
      errorMessage: errorMessage
    );
  }

  RegisterState update({
    bool? isEmailValid,
    bool? isPasswordValid,
    bool? isEmailTaken,
    String? errorMessage
  }) {
    return copyWith(
      isEmailTaken: isEmailTaken,
      isEmailValid: isEmailValid,
      isPasswordValid: isPasswordValid,
      errorMessage: errorMessage,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
    );
  }

  RegisterState copyWith({
    bool? isEmailValid,
    bool? isEmailTaken,
    bool? isPasswordValid,
    bool? isSubmitEnabled,
    bool? isSubmitting,
    bool? isSuccess,
    bool? isFailure,
    String? errorMessage
  }) {
    return RegisterState(
      isEmailValid: isEmailValid ?? this.isEmailValid,
      isEmailTaken: isEmailTaken ?? this.isEmailTaken,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      errorMessage: errorMessage
    );
  }

  
  final bool isEmailValid;
  final bool isEmailTaken;
  final bool isPasswordValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final String? errorMessage;

  bool get isFormValid => isEmailValid && isPasswordValid;

}