import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/constants/image_assets.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/strings.dart';
import 'package:sike/widgets/sike_text.dart';

class SplashScreen extends StatelessWidget {
  SplashScreen({ ScreenResponseHelper? screenResponseHelper}): _screenResponseHelper = screenResponseHelper ?? GetIt.I<ScreenResponseHelper>();
  final ScreenResponseHelper _screenResponseHelper;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: const BoxConstraints.expand(),
        decoration: const BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SikeText(Strings.sike, fontColor: Colors.red),
            Container(
              constraints: BoxConstraints.expand(height: _screenResponseHelper.getRelativeHeight(183), width: _screenResponseHelper.getRelativeWidth(210)),
            alignment: Alignment.center,child: Image.asset(
                ImageAssets.logo,
                fit: BoxFit.contain,
              )
            ),
          ],
        ),
      ),
    );
  }
}