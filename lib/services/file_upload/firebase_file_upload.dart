import 'dart:async';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/services/file_upload/file_upload.dart';
import 'package:sike/services/file_upload/file_upload_event.dart';
import 'package:sike/strings.dart';

class FirebaseFileUpload extends FileUpload{

  FirebaseFileUpload(File file, String storagePath) : super(file, storagePath);

  final StreamController<FileUploadEvent> _streamController = StreamController<FileUploadEvent>.broadcast();
  final Completer<FileUploadSnapshot> _completer = Completer<FileUploadSnapshot>();

  @override
  Future<FileUploadSnapshot> get onComplete => _completer.future;

  @override
  Stream<FileUploadEvent> upload() {
    final FirebaseStorage storage = GetIt.instance<FirebaseStorage>();
    
    final UploadTask storageUploadTask = storage.ref().child(storagePath).putFile(file);
    storageUploadTask.asStream().listen((TaskSnapshot taskSnapshot) async{

      if(taskSnapshot.state == TaskState.success){
        final String url = await taskSnapshot.ref.getDownloadURL();
        final FileUploadSnapshot snapshot = FileUploadSnapshot(null, 1, url);
        _streamController.add(FileUploadEvent(FileUploadEventType.success, snapshot));
        _completer.complete(snapshot);
        return;
      }

      if(taskSnapshot.state == TaskState.error){
        final FileUploadSnapshot snapshot = FileUploadSnapshot(Strings.fileUploadFailed, 0, null);
        _streamController.add(FileUploadEvent(FileUploadEventType.failure, snapshot));
        _completer.complete(snapshot);
        return;
      }

      _streamController.add(FileUploadEvent(FileUploadEventType.progress, FileUploadSnapshot('', taskSnapshot.bytesTransferred / taskSnapshot.totalBytes, null)));

    });
    return _streamController.stream;
  }
}