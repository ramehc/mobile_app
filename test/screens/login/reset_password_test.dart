import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:sike/blocs/login/login_bloc.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/screens/login/reset_password.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'reset_password_test.mocks.dart';

@GenerateMocks([IAuthenticator])
void main() {
  LiveTestWidgetsFlutterBinding();
  late IAuthenticator authenticator;
  final ScreenResponseHelper screenResponseHelper =
      ScreenResponseHelper(411.42857142857144, 797.7142857142857);
  late ResetPassword resetPassword;
  const String email = 'anyvalidemail@gmail.com';
  final Finder submitButton = find.byType(ElevatedButton);

  setUp(() {
    authenticator = MockIAuthenticator();
    GetIt.I.registerSingleton<ScreenResponseHelper>(screenResponseHelper);
    resetPassword = ResetPassword(
      authenticator: authenticator,
      srh: screenResponseHelper,
    );
  });

  tearDown(() {
    GetIt.I.unregister<ScreenResponseHelper>();
  });

  Widget makeWidgetTestable(Widget? child) {
    return MaterialApp(
      home: Scaffold(body: child),
    );
  }

  Future<void> setEmailField(WidgetTester tester, String email) async {
    final Finder emailField = find.byType(TextFormField).first;

    await tester.tap(emailField);
    await tester.enterText(emailField, email);

    await tester
        .pumpAndSettle(const Duration(milliseconds: LoginBloc.debounceTime));
  }

  group('Reset Password Widget', () {
    group('Submit button is only enabled when email is valid', () {
      testWidgets('Submit button disabled for invalid email',
          (WidgetTester tester) async {
        await tester.pumpWidget(makeWidgetTestable(resetPassword));

        expect(submitButton, findsOneWidget);

        expect(
            tester.widget<ElevatedButton>(submitButton).enabled,
            isFalse);

        await setEmailField(tester, 'anyinvalidemail');

        await expectLater(
            tester.widget<ElevatedButton>(submitButton).enabled,
            isFalse);
      });

      testWidgets('Submit button enabled for valid email',
          (WidgetTester tester) async {
        await tester.pumpWidget(makeWidgetTestable(resetPassword));

        expect(submitButton, findsOneWidget);

        expect(
            tester.widget<ElevatedButton>(submitButton).enabled,
            isFalse);

        await setEmailField(tester, email);

        expect(
            tester.widget<ElevatedButton>(submitButton).enabled,
            isTrue);
      });
    });

    testWidgets('Reset password is called on submit button is pressed',
        (WidgetTester tester) async {
      await tester.pumpWidget(makeWidgetTestable(resetPassword));
      
      expect(submitButton, findsOneWidget);

      expect(tester.widget<ElevatedButton>(submitButton).enabled,
          isFalse);

      await setEmailField(tester, email);

      expect(tester.widget<ElevatedButton>(submitButton).enabled,
          isTrue);

      await tester.tap(submitButton);

      verify(authenticator.sendRestPasswordEmail(email: email)).called(1);
    });
  });
}
