class FileUploadEvent{
  FileUploadEvent(this.type, this.snapShot);
  final FileUploadSnapshot snapShot;
  final FileUploadEventType type;

}

enum FileUploadEventType{
  progress,
  success,
  failure
}

class FileUploadSnapshot{
  FileUploadSnapshot(this.errorMessage, this.percentage, this.url);
  final String? errorMessage;
  final String? url;
  final double percentage;
}