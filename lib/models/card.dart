import 'package:json_annotation/json_annotation.dart';

part 'card.g.dart';

@JsonSerializable()
class SikeCard {

  SikeCard(
      {required this.id,
      required this.title,
      required this.description,
      required this.imageUrl,
      required this.instructions,
      required this.maxResponseCharacters,
      this.assetURI});

  factory SikeCard.fromJson(Map<String, dynamic> json) => _$SikeCardFromJson(json);
  
  final String id;
  final String title;
  final String description;
  final String imageUrl;
  final String? assetURI;
  final int maxResponseCharacters;
  final String instructions;
  Map<String, dynamic> toJson() => _$SikeCardToJson(this);
}
