import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:get_it/get_it.dart';
import 'package:meta/meta.dart';
import 'package:sike/exceptions/error_message_service.dart';
import 'package:sike/models/user.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/services/local_store/preference.dart';
import 'package:sike/services/local_store/storage_service.dart';
import 'package:sike/strings.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc(
      {IAuthenticator? authenticator, ILocalStorageService? localStorageService}):
        _localStorageService =
            localStorageService ?? GetIt.I<ILocalStorageService>(),
        _authenticator = authenticator ?? GetIt.I<IAuthenticator>(),
        super(Uninitialized());

  final IAuthenticator _authenticator;
  final ILocalStorageService _localStorageService;

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    switch (event.runtimeType) {
      case AppStarted:
        yield* _appStarted();
        break;
      case LoggedIn:
        yield* _loggedIn();
        break;
      case LoggedOut:
        yield* _loggedOut();
        break;
      case LoginError:
        yield* _loginError(event as LoginError);
    }
  }

  Stream<AuthenticationState> _appStarted() async* {
    try {

      yield Authenticated(SikeUser(email: 'chemar@gmail.com', id: 'id'));
      return;

      final SikeUser? user = _authenticator.getUser();

      if (user == null) {
        yield Unauthenticated();
        return;
      }

      _saveSikeUserDetails(user);
      yield Authenticated(user);
    } catch (e) {
      yield AuthenticationError(errorMessage: ErrorService.getErrorMessage(e));
    }
  }

  Stream<AuthenticationState> _loggedIn() async* {
    try {
      final SikeUser? user = _authenticator.getUser();

      if (user == null) {
        yield Unauthenticated();
        yield AuthenticationError(errorMessage: Strings.userEmpty);
        return;
      }

      _saveSikeUserDetails(user);
      yield Authenticated(user);
    } catch (e) {
      yield AuthenticationError(
          errorMessage: ErrorService.getErrorMessage(e,
              defaultMessage: Strings.userEmpty));
    }
  }

  void _saveSikeUserDetails(SikeUser user) {
    _localStorageService.saveToLocalStorage(Preference.userId, user.id);
  }

  Stream<AuthenticationState> _loggedOut() async* {
    try {
      if (!await _authenticator.signOut()) {
        yield AuthenticationError(errorMessage: Strings.signOutFailed);
        return;
      }

      yield Unauthenticated();
    } catch (e) {
      yield AuthenticationError(
          errorMessage: ErrorService.getErrorMessage(e,
              defaultMessage: Strings.signOutFailed));
    }
  }

  Stream<AuthenticationState> _loginError(LoginError event) async* {
    yield AuthenticationError(errorMessage: event.errorMessage);
  }
}
