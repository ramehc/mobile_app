import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/constants/image_assets.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/services/image_upload/image_upload.dart';

class UserImageDisplay extends StatelessWidget {
  UserImageDisplay(this.imageType, {Key? key, this.imageUrl = '', this.borders = true, ScreenResponseHelper? srh}):
  srh = srh ?? GetIt.I<ScreenResponseHelper>(),
        super(key: key);
  
  final String imageUrl;
  final ImageType imageType;
  final bool borders;
  final ScreenResponseHelper srh;
  @override
  Widget build(BuildContext context) {
    return imageUrl.isNotEmpty
        ? CachedNetworkImage(
            imageUrl: imageUrl,
            imageBuilder: (BuildContext context, ImageProvider imageProvider) => Container(
              height: _getHeight(),
              width: _getWidth(),
              decoration: BoxDecoration(
                border: borders ? Border.all(color: Colors.indigoAccent, width: 3) : Border.all(style: BorderStyle.none),
                shape: _getShape(),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            placeholder: (BuildContext context, String url) => Container(
              decoration: BoxDecoration(shape: _getShape()),
              height: _getHeight(),
              width: _getWidth(),
              child: const Center(child: CircularProgressIndicator()),
            ),
            errorWidget: (BuildContext context, String url, dynamic error) => Container(
              height: _getHeight(),
              width: _getWidth(),
              decoration: BoxDecoration(shape: _getShape()),
              child: const Center(child: Icon(Icons.error_outline)),
            ),
          )
        : Container(
            height: _getHeight(),
            width: _getWidth(),
            decoration: BoxDecoration(
                border: borders ? Border.all(color: Colors.indigoAccent, width: 3) : Border.all(style: BorderStyle.none),
                shape: _getShape(),
                image:  DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage(imageType == ImageType.sike ? ImageAssets.sikedPicture : ImageAssets.defaultUser))),
            child: Container());
  }

  double _getHeight() {
    return imageType == ImageType.profile
        ? srh.getRelativeWidth(150)
        : srh.getRelativeHeight(300);
  }

  double _getWidth() {
    return imageType == ImageType.profile
        ? srh.getRelativeWidth(150)
        : srh.getRelativeHeight(240);
  }

  BoxShape _getShape() {
    return imageType == ImageType.profile
        ? BoxShape.circle
        : BoxShape.rectangle;
  }
}
