import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/login/login_bloc.dart';
import 'package:sike/widgets/resend_verification_button.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/sike_text_form.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/models/route/navigator_pop_result.dart';
import 'package:sike/models/route/pop_result.dart';
import 'package:sike/constants/routes.dart';
import 'package:sike/helpers/icon/system_icons.dart';
import 'package:sike/strings.dart';
import 'package:sike/theme/font_size.dart';

class EmailLogin extends StatefulWidget {
  EmailLogin(
      {Key? key, required LoginBloc loginBloc, ScreenResponseHelper? srh})
      : _loginBloc = loginBloc,
        _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
        super(key: key);

  final LoginBloc _loginBloc;
  final ScreenResponseHelper _srh;

  @override
  _EmailLoginState createState() => _EmailLoginState();
}

class _EmailLoginState extends State<EmailLogin> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;
  ScreenResponseHelper get _srh => widget._srh;
  LoginBloc get _loginBloc => widget._loginBloc;

  @override
  void initState() {
    super.initState();
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.add(
      PasswordChanged(password: _passwordController.text),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState state) {
          return Form(
              child: Column(children: <Widget>[
            SikeTextFormField(
              controller: _emailController,
              labelText: Strings.email,
              icon: SystemIcon.email,
              autovalidateMode: AutovalidateMode.always,
              autoCorrect: false,
              validator: (_) {
                return !state.isEmailValid ? Strings.invalidEmail : null;
              },
            ),
            SikeTextFormField(
              controller: _passwordController,
              labelText: Strings.password,
              icon: SystemIcon.lock,
              autovalidateMode: AutovalidateMode.always,
              autoCorrect: false,
              obscureText: true,
              validator: (_) {
                return !state.isPasswordValid ? Strings.invalidPassword : null;
              },
            ),
            _errorMessage(state),
            Container(
                margin: EdgeInsets.only(top: _srh.getRelativeHeight(10)),
                child: ElevatedButton(
                    onPressed:
                        _isSignInButtonEnabled(state) ? _onFormSubmitted : null,
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    )),
                    child: SikeText(Strings.signIn))),
            GestureDetector(
              onTap: () {
                _resetPassword(context);
              },
              child: Container(
                height: _srh.getRelativeHeight(30),
                margin: EdgeInsets.only(
                    right: _srh.getRelativeWidth(10),
                    top: _srh.getRelativeHeight(20)),
                child: Text(
                  Strings.forgotPassword,
                  style: TextStyle(
                      color: Colors.blueAccent,
                      decoration: TextDecoration.underline,
                      fontSize: _srh.getRelativeFontSize(FontSize.bodyText)),
                ),
              ),
            )
          ]));
        });
  }

  Widget _errorMessage(LoginState state) {
    if (state.isTooManyAttempts) {
      return _errorMessageContainer(Strings.tooManySignIn);
    }

    if (state.isUserDisabled) {
      return _errorMessageContainer(Strings.accountDisabled);
    }

    if (state.isUserNotFound) {
      return _errorMessageContainer(Strings.userNotFound);
    }

    if (state.isPasswordWrong) {
      return _errorMessageContainer(Strings.incorrectPassword);
    }

    if (state.isEmailNotVerified) {
      return Column(
        children: <Widget>[
          _errorMessageContainer(Strings.emailNotVerified),
          ResendVerificationLink()
        ],
      );
    }

    return Container();
  }

  Widget _errorMessageContainer(String message) {
    return Container(
        margin: EdgeInsets.only(top: _srh.getRelativeHeight(10)),
        child: Text(
          message,
          maxLines: 2,
          textAlign: TextAlign.right,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: Colors.red,
              fontSize: _srh.getRelativeFontSize(FontSize.bodyText)),
        ));
  }

  bool _isSignInButtonEnabled(LoginState state) {
    return state.isFormValid && !state.isSubmitting && isPopulated;
  }

  void _onFormSubmitted() {
    _loginBloc.add(
      LoginWithCredentials(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }

  void _resetPassword(BuildContext context) {
    Navigator.of(context)
        .pushNamed(Routes.resetPassword)
        .then((Object? response) {
      if (response == null) {
        return;
      }

      if (response.runtimeType != NavigatorPopResult) {
        return;
      }

      final NavigatorPopResult resetResponse = response as NavigatorPopResult;

      if (resetResponse.result == PopResult.failure) {
        return;
      }

      ScaffoldMessenger.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(Toast.success(resetResponse.displayMessage));
    });
  }
}
