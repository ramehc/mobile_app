import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/exceptions/error_message_service.dart';
import 'package:sike/exceptions/exceptions.dart';
import 'package:sike/models/game/game_user.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/services/local_store/preference.dart';
import 'package:sike/services/local_store/storage_service.dart';
import 'package:sike/strings.dart';

enum UserSetting{displayName, profilePicture, userId}
class UserSettingsService{

  late ILocalStorageService _localStore;
  late AuthenticationBloc _authenticationBloc;
  late IAuthenticator _authenticator;

  UserSettingsService({ILocalStorageService? localStorageService, AuthenticationBloc? authenticationBloc, IAuthenticator? authenticator}){
    _authenticator = authenticator ?? GetIt.I<IAuthenticator>();
    _localStore = localStorageService ?? GetIt.I<ILocalStorageService>();
    _authenticationBloc = authenticationBloc ?? GetIt.I<AuthenticationBloc>();
  }

  Future<void> setDisplayName(String displayName) async{
    await _updateInfo(displayName, UserSetting.displayName);
  } 

  Future<void> setProfilePicture(String imageUrl) async{
    await _updateInfo(imageUrl, UserSetting.profilePicture);
  }

  Future<void> setSikePicture(String imageUrl) async{
    _localStore.saveToLocalStorage(Preference.sikePictureUrl, imageUrl);
  }

  String getUserId(){
    return _getInfo(UserSetting.userId);
  }

  String getDisplayName() {
    return _getInfo(UserSetting.displayName);
  }

  String getProfilePicture() {
    return _getInfo(UserSetting.profilePicture);
  }

  String getSikePicture(){
    if(!_localStore.valueExist(Preference.sikePictureUrl)){
      return '';
    }

    return _localStore.getFromLocalStorage(Preference.sikePictureUrl).toString();
  }

  GameUser getGameUser() {
    try{
      final User? currentUser = FirebaseAuth.instance.currentUser;

      if(currentUser == null){
        _authenticationBloc.add(LoggedOut());
        throw UserError(Strings.notSignedIn);
      }

      return GameUser(
        id: currentUser.email!, 
        displayName:  getDisplayName(), 
        profilePictureUrl:  getProfilePicture(), 
        sikePictureUrl: getSikePicture());


    }catch(e){

      if(e is UserError){
        rethrow;
      }

      final String errorMessage = ErrorService.getErrorMessage(e);
      throw UserError(errorMessage);
    }
  }

  String _getInfo(UserSetting setting) { //TODO remove dependency on Firebase Auth instance
    try {
      final User? currentUser =  FirebaseAuth.instance.currentUser;

      if(currentUser == null){
        _authenticationBloc.add(LoggedOut());
        throw UserError(Strings.notSignedIn);
      }

      switch (setting) {
        case UserSetting.displayName:
          return currentUser.displayName == null || currentUser.displayName!.isEmpty ? 'SikeUser${currentUser.uid.hashCode}' : currentUser.displayName!;
        case UserSetting.profilePicture:
          return  currentUser.photoURL ?? '';
        case UserSetting.userId:
          return currentUser.uid;
        default:
          throw Exception('Not implemented');
      }
      
    } catch (e) {
      if(e is UserError){
        rethrow;
      }

      final String errorMessage = ErrorService.getErrorMessage(e);
      throw UserError(errorMessage);
    }
    
  
  }

  Future<void> _updateInfo(String value, UserSetting setting) async{ //TODO remove dependency on Firebase Auth instance
    try {
      final User? currentUser = FirebaseAuth.instance.currentUser;

      if(currentUser == null){
        _authenticationBloc.add(LoggedOut());
        throw UserError(Strings.notSignedIn);
      }
      
      switch (setting) {
        case UserSetting.displayName:
          await currentUser.updateProfile(displayName: value);
          _localStore.saveToLocalStorage(Preference.displayName, value);
          break;
        case UserSetting.profilePicture:
          await currentUser.updateProfile(photoURL: value);
          _localStore.saveToLocalStorage(Preference.profilePictureUrl, value);
          break;
        case UserSetting.userId:
          _localStore.saveToLocalStorage(Preference.userId, value);
      }
    } catch (e) {
      if(e is UserError){
        rethrow;
      }

      final String errorMessage = ErrorService.getErrorMessage(e);
      throw UserError(errorMessage);
    }
  }
}