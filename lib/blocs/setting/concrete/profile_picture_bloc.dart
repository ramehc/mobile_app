import 'package:sike/blocs/setting/setting_bloc.dart';
import 'package:sike/exceptions/error_message_service.dart';
import 'package:sike/services/user_settings/settings_service.dart';

class ProfilePictureBloc extends SettingBloc{
  ProfilePictureBloc({UserSettingsService? userSettings}) : super(userSettings);


  @override
  Stream<SettingState> settingChanged(SettingChanged event) async*{
    try {
      yield const SettingLoading();
      await userSettings.setProfilePicture(event.value.toString());
      yield SettingLoaded(event.value.toString());
    } catch (e) {
      yield SettingError(ErrorService.getErrorMessage(e));
    }
    

  }

  @override
  Stream<SettingState> getSetting() async*{
    try {
      yield SettingLoaded(userSettings.getProfilePicture());
    } catch (e) {
      yield SettingError(ErrorService.getErrorMessage(e));
    }
    
  }

}