import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/helpers/icon/system_icons.dart';
import 'package:sike/widgets/sike_text.dart';

enum MessageType{success, fail, loading}
class Toast{
  static SnackBar error(String errorMessage) {
    return _snackBar(errorMessage, MessageType.fail);
  }

  static SnackBar success(String successMessage){
    return _snackBar(successMessage, MessageType.success);
  }

  static SnackBar loading(String loadingMessage){
    return _snackBar(loadingMessage, MessageType.loading);
  }

  static SnackBar _snackBar(String message, MessageType type){
    final srh = GetIt.I<ScreenResponseHelper>();
    return SnackBar(
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            width: srh.getPercentageWidth(0.75),
            child: SikeText(
              message,
              fontColor: Colors.white
            ),
          ),
          if (type == MessageType.loading) SizedBox(width:  srh.getPercentageWidth(0.05), height: srh.getPercentageWidth(0.05), child: const CircularProgressIndicator()) else Icon(_icon(type), color: Colors.white,)
        ],
      ),
      backgroundColor: _color(type),
    );
  }

  static Color _color(MessageType type){
    switch (type) {
      case MessageType.success:
        return Colors.green;
      case MessageType.fail:
        return Colors.red;
      default:
        return const Color(0xFFBDBDBD);
    }
  }

  static IconData _icon(MessageType type){
    switch (type) {
      case MessageType.success:
        return SystemIcon.check;
      case MessageType.fail:
        return SystemIcon.error;
      default:
       return Icons.error;
    }
  }
}