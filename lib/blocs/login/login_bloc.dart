import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/helpers/validators.dart';
import 'package:sike/constants/authentication_errors.dart';
import 'package:sike/exceptions/exceptions.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/strings.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({
    IAuthenticator? authenticator,
  })  : _authenticator = authenticator ?? GetIt.I<IAuthenticator>(),
        super(LoginState.empty());

  final IAuthenticator _authenticator;
  static const debounceTime = 300;

  @override
  Stream<Transition<LoginEvent, LoginState>> transformEvents(
    Stream<LoginEvent> events,
    TransitionFunction<LoginEvent, LoginState> transitionFn,
  ) {
    final Stream<LoginEvent> nonDebounceStream =
        events.where((LoginEvent event) {
      return event is! EmailChanged && event is! PasswordChanged;
    });
    final Stream<LoginEvent> debounceStream = events.where((LoginEvent event) {
      return event is EmailChanged || event is PasswordChanged;
    }).debounceTime(const Duration(milliseconds: LoginBloc.debounceTime));
    return super.transformEvents(
        nonDebounceStream.mergeWith(<Stream<LoginEvent>>[debounceStream]),
        transitionFn);
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    switch (event.runtimeType) {
      case EmailChanged:
        yield* _emailChanged((event as EmailChanged).email);
        break;
      case PasswordChanged:
        yield* _passwordChanged((event as PasswordChanged).password);
        break;
      case LoginWithGoogle:
        yield* _loginWithGoogle();
        break;
      case LoginWithCredentials:
        yield* _loginWithCredentials(
            (event as LoginWithCredentials).email, event.password);
        break;
      case LoginWithFacebook:
        yield* _loginWithFacebook();
        break;
    }
  }

  Stream<LoginState> _emailChanged(String email) async* {
    yield state.update(
        isEmailValid: Validators.isValidEmail(email),
        isUserDisabled: false,
        isUserNotFound: false,
        isTooManyAttempts: false,
        isEmailNotVerified: false);
  }

  Stream<LoginState> _passwordChanged(String password) async* {
    yield state.update(
        isPasswordValid: Validators.isValidPassword(password),
        isPasswordWrong: false,
        isEmailNotVerified: false);
  }

  Stream<LoginState> _loginWithGoogle() async* {
    try {
      yield LoginState.loading();
      if (await _authenticator.signInWithGoogle()) {
        yield LoginState.success();
        return;
      }
      yield LoginState.failure();
    } catch (e) {
      if(e is AuthenticationException){
        yield LoginState.failure(errorMessage: e.message);
        return;
      }

      yield LoginState.failure(errorMessage: 'Login failed, try again.');
    }
  }

  Stream<LoginState> _loginWithFacebook() async* {
    try {
      yield LoginState.loading();
      if (await _authenticator.signInWithFacebook()) {
        yield LoginState.success();
        return;
      }
      yield LoginState.failure();
    } catch (e) {
      if(e is AuthenticationException){
        yield LoginState.failure(errorMessage: e.message);
        return;
      }

      yield LoginState.failure(errorMessage: 'Login failed, try again.');
    }
  }

  Stream<LoginState> _loginWithCredentials(
    String email,
    String password,
  ) async* {
    yield LoginState.loading();
    try {
      if (await _authenticator.signInWithCredentials(email, password)) {
        yield LoginState.success();
        return;
      }
      yield LoginState.failure();
    } catch (e) {

      if (e is PlatformException) {
        switch (e.code) {
          case AuthenticationErrors.wrongPassword:
            yield state.update(isPasswordValid: false, isPasswordWrong: true, errorMessage: e.message);
            break;
          case AuthenticationErrors.userDisabled:
            yield state.update(isUserDisabled: true, errorMessage: e.message);
            break;
          case AuthenticationErrors.invalidEmail:
            yield state.update(isEmailValid: false, errorMessage: e.message);
            break;
          case AuthenticationErrors.userNotFound:
            yield state.update(isEmailValid: false, isUserNotFound: true, errorMessage: e.message);
            break;
          case AuthenticationErrors.tooManyAttempts:
            yield state.update(isTooManyAttempts: true, errorMessage: e.message);
            break;
          default:
            yield LoginState.failure(errorMessage: e.message);
        }
        return;
      }

      if (e == EmailNotVerifiedException) {
        yield state.update(isEmailNotVerified: true, errorMessage: Strings.emailNotVerified);
        return;
      }

      yield LoginState.failure(errorMessage: 'Login failed, try again.');
    }
  }
}