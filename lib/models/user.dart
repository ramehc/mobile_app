import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class SikeUser {
  SikeUser(
      {required this.email,
      required this.id,
      String? displayName,
      String? profilePictureUrl,
      String? sikePictureUrl})
      : displayName = displayName ?? '',
        profilePictureUrl = profilePictureUrl ?? '',
        sikePictureUrl = sikePictureUrl ?? '';

  factory SikeUser.fromJson(Map<String, dynamic> json) => _$SikeUserFromJson(json);

  String email;
  String id;
  String displayName = '';
  String profilePictureUrl = '';
  String sikePictureUrl = '';

  Map<String, dynamic> toJson() => _$SikeUserToJson(this);
}
