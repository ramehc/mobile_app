import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/blocs/setting/concrete/display_name_bloc.dart';
import 'package:sike/blocs/setting/setting_bloc.dart';
import 'package:sike/theme/font_size.dart';
import 'package:sike/widgets/sike_text.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/services/user_settings/settings_service.dart';
import 'package:sike/strings.dart';

class DisplayName extends StatefulWidget {
  DisplayName({
    Key? key,
    DisplayNameBloc? displayNameBloc,
    ScreenResponseHelper? srh,
    UserSettingsService? userSettingsService
  }) :
  _displayNameBloc = displayNameBloc ?? GetIt.I<DisplayNameBloc>(),
  _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
  _userSettingsService = userSettingsService ?? GetIt.I<UserSettingsService>(),
  super(key: key);

  final DisplayNameBloc _displayNameBloc;
  final ScreenResponseHelper _srh;
  final UserSettingsService _userSettingsService;

  @override
  _DisplayNameState createState() => _DisplayNameState();
}

class _DisplayNameState extends State<DisplayName> {
  DisplayNameBloc get  _displayNameBloc => widget._displayNameBloc;
  late FocusNode _displayNameFocusNode;
  late TextEditingController _displayNameController;
  ScreenResponseHelper get _srh => widget._srh;
  UserSettingsService get _userSettingsService => widget._userSettingsService;

  @override
  void initState() {
    super.initState();
    _displayNameController = TextEditingController();

    if (_displayNameBloc.state is SettingLoaded) {
      _displayNameController.text =
          _displayNameBloc.state.value.toString();
    } else {
      _displayNameBloc.add(SettingInitialized());
    }

    _displayNameFocusNode = FocusNode();
    _displayNameFocusNode.addListener(_focusListener);
  }

  void _focusListener() {
    if (!_displayNameFocusNode.hasFocus) {
      if (_displayNameController.text == '') {
        //TODO general dialog for both platforms
        showDialog(
          context: context,
          builder: (BuildContext context) {
              return AlertDialog(
                content: SikeText(Strings.displayNameEmpty),
                title:  Text(Strings.displayNameInvalid, style: TextStyle(
                  fontSize: _srh.getRelativeFontSize(FontSize.itemTitle),
                  fontWeight: FontWeight.bold
                )),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: SikeText(Strings.close, fontColor: Colors.blueAccent,)
                  ),
                ],
              );
            }
        );

        if (_displayNameBloc.state is SettingLoaded) {
          _displayNameController.text = _displayNameBloc.state.value.toString();
          return;
        }


        _displayNameController.text = _userSettingsService.getDisplayName();
        return;
      }
      _displayNameBloc.add(SettingChanged(_displayNameController.text));
    }
  }

  @override
  void dispose() {
    _displayNameController.dispose();
    _displayNameFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<DisplayNameBloc, SettingState>(
      bloc: _displayNameBloc,
      listener: (BuildContext context, SettingState state) {
        if (state is SettingError) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(Toast.error(state.errorMessage));
        }
      },
      child: BlocBuilder<DisplayNameBloc, SettingState>(
        bloc: _displayNameBloc,
        builder: (BuildContext context, SettingState state) {
          return SizedBox(
            width: _srh.getPercentageWidth(0.6),
            child: TextField(
              style: TextStyle(
                fontSize: _srh.getRelativeFontSize(FontSize.bodyText)
              ),
              controller: _displayNameController,
              focusNode: _displayNameFocusNode,
              decoration: InputDecoration(
                
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0))),
            ),
          );
        },
      ),
    );
  }
}