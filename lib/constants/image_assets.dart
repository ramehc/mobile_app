abstract class ImageAssets{
  static const String connectionFailed = 'assets/images/connection_failed.png';
  static const String logo = 'assets/images/logo.png';
  static const String defaultUser = 'assets/images/default_user.png';
  static const String connecting = 'assets/images/link.png';
  static const String firstPlace = 'assets/images/awards/first.png';
  static const String secondPlace = 'assets/images/awards/second.png';
  static const String thirdPlace = 'assets/images/awards/third.png';
  static const String sikedPicture = 'assets/images/siked.png';
  static const String disconnected = 'assets/images/disconnected.png';
}