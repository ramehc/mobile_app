import 'dart:io';
import 'package:sike/services/file_upload/file_upload_event.dart';


abstract class FileUpload{
  FileUpload(this.file, this.storagePath);
  final File file;
  final String storagePath;
  Stream<FileUploadEvent> upload();
  Future<FileUploadSnapshot> get onComplete;
}