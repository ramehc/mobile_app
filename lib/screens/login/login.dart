import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:sike/constants/image_assets.dart';
import 'package:sike/blocs/authentication/authentication_bloc.dart';
import 'package:sike/blocs/login/login_bloc.dart';
import 'package:sike/helpers/screen_response_helper.dart';
import 'package:sike/widgets/toast.dart';
import 'package:sike/constants/routes.dart';
import 'package:sike/screens/login/email_login.dart';
import 'package:sike/services/authentication/authenticator.dart';
import 'package:sike/strings.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen(
      {Key? key,
      IAuthenticator? authenticator,
      AuthenticationBloc? authenticationBloc,
      ScreenResponseHelper? srh})
      : _authenticator = authenticator ?? GetIt.I<IAuthenticator>(),
        _authenticationBloc =
            authenticationBloc ?? GetIt.I<AuthenticationBloc>(),
        _srh = srh ?? GetIt.I<ScreenResponseHelper>(),
        super(key: key);

  final IAuthenticator _authenticator;
  final AuthenticationBloc _authenticationBloc;
  final ScreenResponseHelper _srh;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late LoginBloc _loginBloc;
  IAuthenticator get _authenticator => widget._authenticator;
  AuthenticationBloc get _authenticationBloc => widget._authenticationBloc;
  ScreenResponseHelper get _srh => widget._srh;

  @override
  void initState() {
    super.initState();
    _loginBloc = LoginBloc(authenticator: _authenticator);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocListener<AuthenticationBloc, AuthenticationState>(
            bloc: _authenticationBloc,
            listener: (BuildContext context, AuthenticationState state) {
              if (!ModalRoute.of(context)!.isCurrent) {
                return;
              }

              if (state is AuthenticationError) {
                ScaffoldMessenger.of(context)
                  ..hideCurrentSnackBar()
                  ..showSnackBar(Toast.error(state.errorMessage));
              }
            },
            child: BlocListener<LoginBloc, LoginState>(
              bloc: _loginBloc,
              listener: (BuildContext context, LoginState state) {
                if (state.isSubmitting) {
                  ScaffoldMessenger.of(context)
                    ..hideCurrentSnackBar()
                    ..showSnackBar(Toast.loading(Strings.signingIn));
                  return;
                }

                if(state.errorMessage != null){
                  ScaffoldMessenger.of(context)..hideCurrentSnackBar()..showSnackBar(Toast.error(state.errorMessage!));
                }

                if (state.isSuccess) {
                  GetIt.instance<AuthenticationBloc>().add(LoggedIn());
                }
              },
              child: BlocBuilder<LoginBloc, LoginState>(
                bloc: _loginBloc,
                builder: (BuildContext context, LoginState state) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        bottom: 20.0, left: 20, right: 20),
                    child: Align(
                      child: _loginForm()),
                  );
                },
              ),
            )),
      ),
    );
  }

  ListView _loginForm() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        _logo(),
        Row(
          children: <Widget>[
            _facebookSignInButton(),
            const Spacer(),
            _googleSignInButton()
          ],
        ),
        Center(
          child: SizedBox(
            width: _srh.getPercentageWidth(0.8),
            child: const Divider(
              color: Colors.black,
            ),
          ),
        ),
        EmailLogin(loginBloc: _loginBloc),
        _register()
      ]
    );
  }

  Widget _facebookSignInButton() {
    return Container(
      padding: EdgeInsets.only(left: _srh.getPercentageWidth(0.1)),
      width: _srh.getPercentageWidth(0.42),
      child: TextButton(
          onPressed: () => _loginBloc.add(LoginWithFacebook()),
          style: ElevatedButton.styleFrom(
            primary: const Color.fromARGB(255, 66, 103, 178),
            onPrimary: Colors.white,
          ),
          child: Text('Facebook',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: _srh.getRelativeFontSize(17)))),
    );
  }

  Widget _googleSignInButton() {
    return Container(
      padding: EdgeInsets.only(right: _srh.getPercentageWidth(0.1)),
      width: _srh.getPercentageWidth(0.42),
      child: TextButton(
          onPressed: () => _loginBloc.add(LoginWithGoogle()),
          style: ElevatedButton.styleFrom(
              primary: const Color.fromARGB(255, 219, 50, 54),
              onPrimary: Colors.white),
          child: Text('Google',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: _srh.getRelativeFontSize(17)))),
    );
  }

  Widget _register() {
    return GestureDetector(
      onTap: () => Navigator.of(context)
          .pushNamed(Routes.register),
      child: Padding(
        padding: EdgeInsets.only(top: _srh.getRelativeHeight(80)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                constraints: BoxConstraints(maxWidth: _srh.getPercentageWidth(0.7)),
                margin: EdgeInsets.only(left: _srh.getRelativeWidth(20)),
                child: Text(Strings.signUpPrompt,
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: _srh.getRelativeFontSize(15)))),
            Container(
                margin: EdgeInsets.only(left: _srh.getRelativeWidth(10)),
                child: const Icon(
                  Icons.arrow_forward
                ))
          ],
        ),
      ),
    );
  }

  Widget _logo() {
    return Padding(
      padding: EdgeInsets.only(bottom: _srh.getRelativeHeight(20)),
      child: SizedBox(
        width: _srh.getPercentageWidth(0.45),
        height: _srh.getRelativeHeight(92),
        child: Image.asset(
          ImageAssets.logo,
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _loginBloc.close();
    super.dispose();
  }
}
