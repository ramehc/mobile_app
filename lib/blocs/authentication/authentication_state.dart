part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationState extends Equatable{}

class Uninitialized extends AuthenticationState {
  @override
  List<Object> get props => [];
}

class Authenticated extends AuthenticationState {
  Authenticated(this.currentUser);
  final SikeUser currentUser;

  @override
  List<Object> get props => [currentUser];

}

class Unauthenticated extends AuthenticationState {
  @override
  List<Object> get props => [];
}

class AuthenticationError extends AuthenticationState{
  AuthenticationError({required this.errorMessage});
  final String errorMessage;

  @override
  List<Object> get props => [errorMessage];
}


