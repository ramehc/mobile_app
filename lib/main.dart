import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sike/app_startup_service.dart';
import 'package:sike/sike.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await setupServiceLocator();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) =>
    runApp(
      DevicePreview(
        builder: (context) => App()
        ))
    );
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Sike();
  }
}